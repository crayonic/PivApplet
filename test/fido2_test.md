# Create venv
Use path to python.exe (preferred version 3.9, because no `swig` install is needed)

Use whatever venv name you prefer. We use `venvtest` here

`py -m virtualenv venvtest`

# Activate venv
`venvtest\Scripts\activate`

# Install requirements
`pip install -r requirements.txt`

# Install patched python-fido2 library from local dir, because it is not public on PyPI
`pip install -e thirdparty\python-fido2`

# Run the test
`python fido2_test.py`