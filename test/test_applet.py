import pytest
import smartcard
import os
import hmac
import hashlib
import random
import string

from binascii import hexlify, unhexlify

import cryptography
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec, utils, padding
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, load_der_public_key
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from fido_applet import FidoApplet
from fido_applet import PivApplet
from fido_applet import FidoAppletUserKey

backend = default_backend()
curve = ec.SECP256R1()

from test_base import *

DERIVATION_CONSTANT_FIDO_MASTER = b'FIDO_MASTER'
DERIVATION_CONSTANT_FIDO_MASTER2 = b'FIDO_MASTER2'
DERIVATION_CONSTANT_FIDO_TRANSPORT = b'FIDO_TRANSPORT'
DERIVATION_CONSTANT_ENCRYPTION_KEYS = b'CRYPTO_SECRET'

DERIVATION_CONSTANT_USER_KEY_HMACSECRET = b'\x00'
DERIVATION_CONSTANT_USER_KEY_AES256 = b'\x01'

def test_applet_connection(unauthorized_applet: FidoApplet):
    pass

def test_reset_piv(applet: FidoApplet):

    # entropy = os.urandom(ENTROPY_LENGTH)
    # response = applet.importEntropy(entropy, allowExport=True)
    # assert_sw(response, ISO7816.SW_NO_ERROR)

    # derivation_bytes = os.urandom(50)
    # response = applet.u2fMakeAuthTag(derivation_bytes) 
    # assert_sw(response, ISO7816.SW_NO_ERROR)
    # tag_before_reset = response.body

    for _ in range(1):
        PIN_LEN = 8
        # Deplete PINS
        for _ in range(5):
            randomPin = ''.join(random.choice(string.digits) for _ in range(PIN_LEN))
            response = applet.authPin(randomPin)
            print(response)
        
        PUK_LEN = 8
        for _ in range(3):
            randomPuk = ''.join(random.choice(string.digits) for _ in range(PUK_LEN))
            response = applet.unblockPin(randomPuk, "123456")
            print(response)

        response = applet.reset()
        assert_sw(response, ISO7816.SW_NO_ERROR)
        assert response.body[:2] == b'\x99\x08'
        assert len(response.body[2:]) == 8
        newPuk = response.body[2:].decode()
        print("New PUK:", newPuk)

        response = applet.authPin("123456")

        entropy = os.urandom(ENTROPY_LENGTH)
        assert_sw(response, ISO7816.SW_NO_ERROR)
        response = applet.importEntropy(entropy, allowExport=True)
        assert_sw(response, ISO7816.SW_NO_ERROR)

        response = applet.importEntropy(entropy, allowExport=True)
        assert_sw(response, ISO7816.SW_NO_ERROR)

        response = applet.exportEntropy()
        assert_sw(response, ISO7816.SW_NO_ERROR)
        assert response.body == entropy

    return

    response = applet.unblockPin(newPuk, "123456")
    print(response)

    applet.authPin("123456")
    response = applet.u2fMakeAuthTag(derivation_bytes) 
    assert_sw(response, ISO7816.SW_NO_ERROR)
    tag_after_reset = response.body

    assert tag_after_reset != tag_before_reset

    # Deplete PINS
    for _ in range(5):
        randomPin = ''.join(random.choice(string.digits) for _ in range(PIN_LEN))
        response = applet.authPin(randomPin)
        print(response)

    randomPuk = ''.join(random.choice(string.digits) for _ in range(PUK_LEN))
    

    response = applet.unblockPin(newPuk, "123456")
    assert_sw(response, ISO7816.SW_NO_ERROR)

def test_generate_entropy(applet: FidoApplet):
    # Fiddling with the parameters
    response = applet.generateEntropy()
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)
    
    response = applet.generateEntropy(allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)

    entropies = set()
    
    response = applet.generateEntropy(exportNow=True)
    assert len(response) == ENTROPY_LENGTH
    entropies.add(str(response))
    assert_sw(response, ISO7816.SW_NO_ERROR)

    response = applet.generateEntropy(allowExport=True, exportNow=True)
    assert len(response) == ENTROPY_LENGTH
    entropies.add(str(response))
    assert_sw(response, ISO7816.SW_NO_ERROR)

    # Entropies are unique
    assert len(entropies) == 2


def test_import_entropy(applet: FidoApplet):
    entropy = os.urandom(ENTROPY_LENGTH)
    response = applet.importEntropy(entropy)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)
    
    entropy = os.urandom(ENTROPY_LENGTH)
    response = applet.importEntropy(entropy, allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)

    entropy = os.urandom(0)
    response = applet.importEntropy(entropy, allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)

    entropy = os.urandom(ENTROPY_LENGTH + 1)
    response = applet.importEntropy(entropy, allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)

    entropy = os.urandom(ENTROPY_LENGTH - 1)
    response = applet.importEntropy(entropy, allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)


def test_export_entropy(applet: FidoApplet):
    entropy = os.urandom(ENTROPY_LENGTH)
    applet.importEntropy(entropy)
    response = applet.exportEntropy()
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED)
    
    entropy = os.urandom(ENTROPY_LENGTH)
    applet.importEntropy(entropy, allowExport=True)
    response = applet.exportEntropy()
    assert len(response) == ENTROPY_LENGTH
    assert response.body == entropy
    assert_sw(response, ISO7816.SW_NO_ERROR)

def make_auth_tag(key, derivation_data):
    return hmac.new(key, derivation_data, hashlib.sha256).digest()[:AUTH_TAG_LENGTH]

def test_ctap_make_auth_tag(applet: FidoApplet):
    master = os.urandom(ENTROPY_PART_LENGTH)
    master2 = os.urandom(ENTROPY_PART_LENGTH)
    transport = os.urandom(ENTROPY_PART_LENGTH)
    derivation_bytes = os.urandom(50)
    tag = make_auth_tag(transport, derivation_bytes)
    entropy = master + master2 + transport
    applet.importEntropy(entropy)
    response = applet.ctapMakeAuthTag(derivation_bytes)
    assert hexlify(response.body) == hexlify(tag)

def test_u2f_make_auth_tag(applet: FidoApplet):
    master = os.urandom(ENTROPY_PART_LENGTH)
    master2 = os.urandom(ENTROPY_PART_LENGTH)
    transport = os.urandom(ENTROPY_PART_LENGTH)
    derivation_bytes = os.urandom(50)
    tag = make_auth_tag(master, derivation_bytes)
    entropy = master + master2 + transport
    applet.importEntropy(entropy)
    response = applet.u2fMakeAuthTag(derivation_bytes)
    assert hexlify(response.body) == hexlify(tag)

def sha256(message):
    digest = hashes.Hash(hashes.SHA256())
    digest.update(message)
    return digest.finalize()

def derive_user_encryption_key(master, derivation_path):
    # print(hexlify(master))
    master = sha256(master)
    # print(hexlify(master))
    for derivation_bytes in derivation_path:
        master = hmac.new(
            master, 
            derivation_bytes, 
            hashlib.sha256
        ).digest()

    return master

def test_derive_user_encryption_key(applet: FidoApplet):
    master_secret = os.urandom(MASTER_SECRET_MAX_LENGTH)
    applet.importMasterSecret(master_secret)

    message = os.urandom(16*6)
    aes_iv = os.urandom(16)
    derivation_bytes = ''.join(random.choices(string.ascii_uppercase + string.digits, k=17)).encode()
    user_encryption_key = derive_user_encryption_key(
        master_secret,
        [
            DERIVATION_CONSTANT_ENCRYPTION_KEYS,
            b'\x01', # wallet id
            DERIVATION_CONSTANT_USER_KEY_AES256,
            derivation_bytes
        ]
    )
    response = applet.deriveUserKeyAES256(
        derivation_bytes,
        message,
        aes_iv
    )
    cipher = Cipher(algorithms.AES(user_encryption_key), modes.CBC(aes_iv))
    encryptor = cipher.encryptor()
    buf = bytearray(len(message) + AES_OUT_LENGTH)
    len_encrypted = encryptor.update_into(message, buf)
    encrypted = buf[:len_encrypted]
    assert hexlify(response.body) == hexlify(encrypted)


    message = os.urandom(16*8)
    aes_iv = os.urandom(16)
    derivation_bytes = ''.join(random.choices(string.ascii_uppercase + string.digits, k=17)).encode()
    user_encryption_key = derive_user_encryption_key(
        master_secret,
        [
            DERIVATION_CONSTANT_ENCRYPTION_KEYS,
            b'\x01', # wallet id
            DERIVATION_CONSTANT_USER_KEY_AES256,
            derivation_bytes
        ]
    )
    response = applet.deriveUserKeyAES256(
        derivation_bytes,
        message,
        aes_iv,
        operation=FidoAppletUserKey.OPERATION_AES256NOPAD_DECRYPT
    )
    cipher = Cipher(algorithms.AES(user_encryption_key), modes.CBC(aes_iv))
    decryptor = cipher.decryptor()
    buf = bytearray(len(message) + AES_OUT_LENGTH)
    len_encrypted = decryptor.update_into(message, buf)
    encrypted = buf[:len_encrypted]
    assert hexlify(response.body) == hexlify(encrypted)


    derivation_bytes = ''.join(random.choices(string.ascii_uppercase + string.digits, k=35)).encode()
    hmac_secret = derive_user_encryption_key(
        master_secret,
        [
            DERIVATION_CONSTANT_ENCRYPTION_KEYS,
            b'\x01', # wallet id
            DERIVATION_CONSTANT_USER_KEY_HMACSECRET,
            derivation_bytes
        ]
    )
    response = applet.deriveUserKeyHmacSecret(
        derivation_bytes
    )
    assert hexlify(response.body) == hexlify(hmac_secret)

    message = os.urandom(16*8)
    aes_iv = os.urandom(16)
    derivation_bytes = ''.join(random.choices(string.ascii_uppercase + string.digits, k=17)).encode()

    # test values outside purpose and operation ranges
    for purpose, operation in [
        (FidoAppletUserKey.PURPOSE_HMACSECRET, 0x01),
        (FidoAppletUserKey.PURPOSE_AES256, 0x00),
        (FidoAppletUserKey.PURPOSE_AES256, 0x03)
    ]:
        response = applet.deriveUserKey(
            purpose,
            operation,
            # valid payload
            [
                0x00, 0x00, 0x01, 0x01, # wallet id 0x01
                0x01, 
                len(derivation_bytes)//256, len(derivation_bytes)%256,
                *derivation_bytes,
                0x02, 
                len(message)//256, len(message)%256,
                *message,
                0x03, 
                len(aes_iv)//256, len(aes_iv)%256,
                *aes_iv
            ]
        )
        assert_sw(response, ISO7816.SW_CONDITIONS_NOT_SATISFIED)

    # test wrong lengths for the above
    response = applet.deriveUserKeyHmacSecret(b'')
    assert_sw(response, ISO7816.SW_WRONG_DATA)
    
    response = applet.deriveUserKeyAES256(b'', message, aes_iv)
    assert_sw(response, ISO7816.SW_WRONG_DATA)
    
    response = applet.deriveUserKeyAES256(derivation_bytes, message, aes_iv)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    
    response = applet.deriveUserKeyAES256(b'', message, aes_iv)
    assert_sw(response, ISO7816.SW_WRONG_DATA)
    
    response = applet.deriveUserKeyAES256(derivation_bytes, b'', aes_iv)
    assert_sw(response, ISO7816.SW_WRONG_DATA)
    
    response = applet.deriveUserKeyAES256(derivation_bytes, b'\x01'*15, aes_iv)
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKeyAES256(derivation_bytes, message, b'')
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKeyAES256(derivation_bytes, message, b'\x01'*15)
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    # test missing parameter
    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_HMACSECRET,
        FidoAppletUserKey.OPERATION_HMACSECRET_SHARE,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            # 0x01, 
            # len(derivation_bytes)//256, len(derivation_bytes)%256,
            # *derivation_bytes,
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            # 0x01, 
            # len(derivation_bytes)//256, len(derivation_bytes)%256,
            # *derivation_bytes,
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *derivation_bytes,
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            # 0x03, 
            # len(aes_iv)//256, len(aes_iv)%256,
            # *aes_iv
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *derivation_bytes,
            # 0x02, 
            # len(message)//256, len(message)%256,
            # *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)
    
    # test multiple parameters
    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *derivation_bytes,
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *derivation_bytes,
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)
    
    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *derivation_bytes,
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    # test longer field than stated
    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv,
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *derivation_bytes,
        ]
    )
    assert_sw(response, ISO7816.SW_NO_ERROR) 

    derivation_bytes_longer = derivation_bytes + b'\xde' 
    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv,
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *derivation_bytes_longer,
        ]
    )
    assert_sw(response, ISO7816.SW_NO_ERROR) # TODO fix this in applet, it should return error

    derivation_bytes_shorter = derivation_bytes[1:]
    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv,
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *derivation_bytes_shorter,
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv,
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *b'\x33',
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv,
            0x01, 
            len(derivation_bytes)//256, len(derivation_bytes)%256,
            *b'',
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv,
            0x01, 
            len(derivation_bytes)//256
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    response = applet.deriveUserKey(
        FidoAppletUserKey.PURPOSE_AES256,
        FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        [
            0x00, 0x00, 0x01, 0x01, # wallet id 0x01
            0x02, 
            len(message)//256, len(message)%256,
            *message,
            0x03, 
            len(aes_iv)//256, len(aes_iv)%256,
            *aes_iv,
            0x01,
        ]
    )
    assert_sw(response, ISO7816.SW_WRONG_DATA)

    


def derive_private_key(master, master2, derivation_bytes):
    hmac_digest = hmac.new(master, derivation_bytes + master, hashlib.sha256).digest()
    # print("hmac_digest", len(hmac_digest), hexlify(hmac_digest).decode())
    cipher = Cipher(algorithms.AES(master2), modes.CBC(b'\x00'*16))
    encryptor = cipher.encryptor()
    buf = bytearray(64)
    len_encrypted = encryptor.update_into(hmac_digest, buf)
    private_key_bytes = buf[:len_encrypted]
    return private_key_bytes

def derive_public_from_private(private_key_bytes):
    priv_key_int = int.from_bytes(private_key_bytes, byteorder='big', signed=False)
    # print(f"private_key_int {priv_key_int:0x}")
    priv_key = ec.derive_private_key(priv_key_int, curve, default_backend())
    pub_key = priv_key.public_key()
    # print('Private key: 0x%x' % priv_key.private_numbers().private_value)
    public_key_bytes = unhexlify(pub_key.public_bytes(serialization.Encoding.X962, serialization.PublicFormat.UncompressedPoint).hex())
    # print(f'Public point (Uncompressed): 0x{public_key}')
    return public_key_bytes

@pytest.fixture
def derived_public_key(applet: FidoApplet):
    master = os.urandom(ENTROPY_PART_LENGTH)
    master2 = os.urandom(ENTROPY_PART_LENGTH)
    transport = os.urandom(ENTROPY_PART_LENGTH)
    # print("entropy", hexlify(master).decode(), hexlify(master2).decode(), hexlify(transport).decode())
    entropy = master + master2 + transport
    derivation_bytes = os.urandom(50)
    
    applet.importEntropy(entropy)

    # for i in range(1000000):
    #     print(i)
    #     response = applet.derivePublicKey(derivation_bytes)
    #     assert_sw(response, ISO7816.SW_NO_ERROR)
    
    response = applet.derivePublicKey(derivation_bytes)
    assert_sw(response, ISO7816.SW_NO_ERROR)

    private_key_bytes = derive_private_key(master, master2, derivation_bytes)
    # print("private_key", len(private_key_bytes), hexlify(private_key_bytes).decode())

    public_key_bytes = derive_public_from_private(private_key_bytes)
    derived_public_key = response.body[:PUBLIC_KEY_LENGTH]
    assert derived_public_key == public_key_bytes
    return derived_public_key, derivation_bytes

def test_derive_public_key(derived_public_key):
    pass


def load_X962_public_key(x962_pk):
    return load_der_public_key(
        unhexlify(
        ("30 59 " # SEQUENCE
            "30 13" # SEQUENCE
                "06 07 2A 86 48 CE 3D 02 01" # OBJECT IDENTIFIER 1.2.840.10045.2.1 ecPublicKey (ANSI X9.62 public key type)
                "06 08 2A 86 48 CE 3D 03 01 07" # OBJECT IDENTIFIER 1.2.840.10045.3.1.7 prime256v1 (ANSI X9.62 named elliptic curve)
        "03 42 00 " # BIT STRING header for public key bytes
        "")
        .replace(" ", "").replace("\r", "").replace("\n", ""))
        + x962_pk
    )


def test_derive_key_and_sign(applet: FidoApplet, derived_public_key: bytes):
    public_key, derivation_bytes = derived_public_key
    print(public_key)

    pub_key = load_X962_public_key(public_key)

    hash = os.urandom(SHA256_LENGTH)
    response = applet.deriveKeyAndSign(hash, derivation_bytes)
    signature = response.body

    # No throw means valid signature
    pub_key.verify(signature, hash, ec.ECDSA(utils.Prehashed(hashes.SHA256())))

    with pytest.raises(cryptography.exceptions.InvalidSignature):
        pub_key.verify(signature, os.urandom(SHA256_LENGTH), ec.ECDSA(utils.Prehashed(hashes.SHA256())))


def test_derive_public_key(derived_public_key: bytes):
    # just ask for derived_public_key is enough
    pass

def test_u2f_new_key(applet: FidoApplet):
    master = os.urandom(ENTROPY_PART_LENGTH)
    master2 = os.urandom(ENTROPY_PART_LENGTH)
    transport = os.urandom(ENTROPY_PART_LENGTH)
    # print("entropy", hexlify(master).decode(), hexlify(master2).decode(), hexlify(transport).decode())
    entropy = master + master2 + transport
    applet.importEntropy(entropy)

    # for i in range(1000000):
    #     print(i)
    #     derivation_bytes = os.urandom(32)
    #     response = applet.u2fNewKey(derivation_bytes)
    #     assert_sw(response, ISO7816.SW_NO_ERROR)

    derivation_bytes = os.urandom(32)
    response = applet.u2fNewKey(derivation_bytes)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    
    assert len(response) == KEY_HANDLE_LENGTH + AUTH_TAG_LENGTH + PUBLIC_KEY_LENGTH or len(response) == KEY_HANDLE_LENGTH + AUTH_TAG_LENGTH + PUBLIC_KEY_LENGTH + 1
    tag_from_applet = response.body[0:AUTH_TAG_LENGTH]
    key_handle = response.body[AUTH_TAG_LENGTH:AUTH_TAG_LENGTH+KEY_HANDLE_LENGTH]
    public_key_bytes = response.body[KEY_HANDLE_LENGTH+AUTH_TAG_LENGTH:KEY_HANDLE_LENGTH+AUTH_TAG_LENGTH+PUBLIC_KEY_LENGTH]
    print(key_handle, public_key_bytes)

    tag = make_auth_tag(master, key_handle + derivation_bytes)
    assert tag == tag_from_applet
    private_key_bytes = derive_private_key(master, master2, b"KV" + tag + key_handle)
    priv_key_int = int.from_bytes(private_key_bytes, byteorder='big', signed=False)
    priv_key = ec.derive_private_key(priv_key_int, curve, default_backend())
    assert hexlify(public_key_bytes).decode() == priv_key.public_key().public_bytes(serialization.Encoding.X962, serialization.PublicFormat.UncompressedPoint).hex()


def test_generate_credential_random(applet: FidoApplet):
    master = os.urandom(ENTROPY_PART_LENGTH)
    master2 = os.urandom(ENTROPY_PART_LENGTH)
    transport = os.urandom(ENTROPY_PART_LENGTH)
    derivation_bytes = os.urandom(33)
    tag = make_auth_tag(transport, derivation_bytes)
    entropy = master + master2 + transport
    applet.importEntropy(entropy)
    response = applet.generateCredentialRandom(derivation_bytes)
    assert hexlify(response.body) == hexlify(tag)

def test_get_credential_mask(applet: FidoApplet):
    master = os.urandom(ENTROPY_PART_LENGTH)
    master2 = os.urandom(ENTROPY_PART_LENGTH)
    transport = os.urandom(ENTROPY_PART_LENGTH)
    derivation_bytes = os.urandom(14)
    tag = make_auth_tag(transport, derivation_bytes)
    entropy = master + master2 + transport
    applet.importEntropy(entropy)
    response = applet.getCredentialMask(derivation_bytes)
    assert hexlify(response.body) == hexlify(tag)

def test_attestation_keypair(applet: FidoApplet):
    response = applet.injectAttestationKeypair(os.urandom(31))
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)

    response = applet.injectAttestationKeypair(os.urandom(33))
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)
    
    private_key = ec.generate_private_key(curve)
    private_key_bytes = private_key.private_numbers().private_value.to_bytes(length=PRIVATE_KEY_LENGTH, byteorder='big')
    # print(hexlify(private_key_bytes))

    response = applet.injectAttestationKeypair(private_key_bytes)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert hexlify(response.body[:PUBLIC_KEY_LENGTH]).decode() == private_key.public_key().public_bytes(serialization.Encoding.X962, serialization.PublicFormat.UncompressedPoint).hex()

    hash = os.urandom(SHA256_LENGTH)
    response = applet.signWithAttestationKey(hash)
    signature = response.body
    private_key.public_key().verify(signature, hash, ec.ECDSA(utils.Prehashed(hashes.SHA256())))
    
    with pytest.raises(cryptography.exceptions.InvalidSignature):
        private_key.public_key().verify(signature, os.urandom(SHA256_LENGTH), ec.ECDSA(utils.Prehashed(hashes.SHA256())))

def test_attestation_keypair_with_static_key(applet: FidoApplet):
    private_key = ec.generate_private_key(curve)
    private_key_bytes = unhexlify("8ddb0c74bb1a1588b4bb4476539c17861efdc4eb420159eac506aaae5ac9a862")
    # print(hexlify(private_key_bytes))
    public_key_bytes = unhexlify("048a5bc66852ba0d6d0c1ea80372d56719a2b130046d2e11fd19dd5fd1d42f7aa6df588e74426ecadffc11fd1db0509096c9c6435863b2058f142129c8988d3d9d")

    response = applet.injectAttestationKeypair(private_key_bytes)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert hexlify(response.body[:PUBLIC_KEY_LENGTH]).decode() == public_key_bytes.hex()

    hash = os.urandom(SHA256_LENGTH)
    response = applet.signWithAttestationKey(hash)
    signature = response.body
    public_key = load_X962_public_key(public_key_bytes)
    public_key.verify(signature, hash, ec.ECDSA(utils.Prehashed(hashes.SHA256())))
    
    with pytest.raises(cryptography.exceptions.InvalidSignature):
        public_key.verify(signature, os.urandom(SHA256_LENGTH), ec.ECDSA(utils.Prehashed(hashes.SHA256())))

    # Try to inject private key with zero length
    response = applet.injectAttestationKeypair(b'')
    # response = applet.injectAttestationKeypair(b'\x00'*PRIVATE_KEY_LENGTH)
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)
    assert len(response.body) == 0

    hash = os.urandom(SHA256_LENGTH)
    response = applet.signWithAttestationKey(hash)
    signature = response.body
    public_key.verify(signature, hash, ec.ECDSA(utils.Prehashed(hashes.SHA256())))

# def test_attestation_endurance_test(applet: FidoApplet):
#     for i in range(1_000_000):
#         private_key = ec.generate_private_key(curve)
#         private_key_bytes = private_key.private_numbers().private_value.to_bytes(length=PRIVATE_KEY_LENGTH, byteorder='big')

#         private_key_bytes = os.urandom(32)

#         response = applet.injectAttestationKeypair(private_key_bytes)
#         assert_sw(response, ISO7816.SW_NO_ERROR)

#         print(i, "signWithAttestationKey")
#         # hash = os.urandom(SHA256_LENGTH)
#         hash = unhexlify("9834876dcfb05cb167a5c24953eba58c4ac89b1adf57f28f2f9d09af107ee8f0")
#         response = applet.signWithAttestationKey(hash)
#         signature = response.body
#         print("private", hexlify(private_key_bytes).decode())
#         print("public", hexlify(derive_public_from_private(private_key_bytes)).decode())
#         print("hash", hexlify(hash).decode())
#         print("signature", hexlify(signature).decode())
#         private_key.public_key().verify(signature, hash, ec.ECDSA(utils.Prehashed(hashes.SHA256())))
    
#     with pytest.raises(cryptography.exceptions.InvalidSignature):
#         private_key.public_key().verify(signature, os.urandom(SHA256_LENGTH), ec.ECDSA(utils.Prehashed(hashes.SHA256())))

#     for i in range(1_000_000):
#         print("test_attestation_endurance_test", i)
#         test_attestation_keypair(applet)

    
def test_generate_random(applet: FidoApplet):
    randoms = set()
    NUMBER_OF_RANDOMS = 5
    for _ in range(NUMBER_OF_RANDOMS):
        response = applet.generateRandom(32)
        assert_sw(response, ISO7816.SW_NO_ERROR)
        randoms.add(response.body)
    assert len(randoms) == NUMBER_OF_RANDOMS

    for length in [1, 15, 33, 64, 79, 127]:
        response = applet.generateRandom(length)
        assert_sw(response, ISO7816.SW_NO_ERROR)
    
def test_entropy_encryption_key(applet: FidoApplet):
    encryption_key = os.urandom(AES256_KEY_LENGTH)
    response = applet.importEntropyEncryptionKey(encryption_key)
    assert_sw(response, ISO7816.SW_NO_ERROR)

    entropy = os.urandom(ENTROPY_LENGTH)
    response = applet.generateEntropy(allowExport=True, exportNow=True)
    assert len(response) == 96
    assert_sw(response, ISO7816.SW_NO_ERROR)
    encryptedEntropyFromExport = response.body

    # Derive reference tag
    derivation_data = os.urandom(50)
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    tag = response.body

    # for i in range(1000000):
    #     print(i)
    #     response = applet.exportEntropy()
    #     assert_sw(response, ISO7816.SW_NO_ERROR)

    response = applet.exportEntropy()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert len(response) == ENTROPY_LENGTH
    encrypted_entropy = response.body
    # Encrypted entropy is same as the one from generation
    assert encryptedEntropyFromExport == encrypted_entropy

    # Tag should be different after regenerate entropy
    response = applet.generateEntropy(allowExport=True, exportNow=True)
    assert len(response) == ENTROPY_LENGTH
    assert_sw(response, ISO7816.SW_NO_ERROR)
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert tag != response.body

    response = applet.importEntropy(encrypted_entropy)    
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)

    # Decrypting entropy, so it should be imported as unencrypted later
    cipher = Cipher(algorithms.AES(encryption_key), modes.CBC(b'\x00'*16))
    decryptor = cipher.decryptor()
    buf = bytearray(256)
    len_encrypted = decryptor.update_into(encrypted_entropy, buf)
    decrypted_entropy = buf[:len_encrypted]
    print('decrypted_entropy', hexlify(decrypted_entropy).decode())

    # Importing same encrypted entropy should derive same tag as reference tag
    applet.importEntropy(encrypted_entropy)
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert tag == response.body

    # After deleting Encryption Key, entropy should be regenerated and derive different tag
    response = applet.deleteEncryptionKey()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert len(response) == 0
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert tag != response.body

    # Tag should be same as reference tag after deleting encryption key and imporing decrypted entropy
    response = applet.deleteEncryptionKey()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert len(response) == 0
    applet.importEntropy(decrypted_entropy)
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert tag == response.body
    
def test_master_secret_encryption_key(applet: FidoApplet):
    encryption_key = os.urandom(AES256_KEY_LENGTH)
    response = applet.importEntropyEncryptionKey(encryption_key)
    assert_sw(response, ISO7816.SW_NO_ERROR)

    master_secret = os.urandom(MASTER_SECRET_MAX_LENGTH)
    response = applet.generateMasterSecret(allowExport=True, exportNow=True)
    assert len(response) == MASTER_SECRET_MAX_LENGTH
    assert_sw(response, ISO7816.SW_NO_ERROR)
    encryptedMasterSecretFromExport = response.body

    # Derive reference user encryption key
    derivation_data_user_key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=15)).encode()
    response = applet.deriveUserKeyHmacSecret(derivation_data_user_key)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    user_encryption_key = response.body

    # Derive reference tag
    derivation_data = os.urandom(50)
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    tag = response.body

    # for i in range(1000000):
    #     print(i)
    #     response = applet.exportMasterSecret()
    #     assert_sw(response, ISO7816.SW_NO_ERROR)

    response = applet.exportMasterSecret()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert len(response) == MASTER_SECRET_MAX_LENGTH
    encryptedMasterSecret = response.body
    # Encrypted master_secret is same as the one from generation
    assert encryptedMasterSecretFromExport == encryptedMasterSecret

    # Tag should be different after regenerate master_secret
    response = applet.generateMasterSecret(allowExport=True, exportNow=True)
    assert len(response) == MASTER_SECRET_MAX_LENGTH
    assert_sw(response, ISO7816.SW_NO_ERROR)
    response = applet.deriveUserKeyHmacSecret(derivation_data_user_key)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert user_encryption_key != response.body
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert tag != response.body

    response = applet.importMasterSecret(encryptedMasterSecret)    
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)

    # Decrypting master_secret, so it should be imported as unencrypted later
    cipher = Cipher(algorithms.AES(encryption_key), modes.CBC(b'\x00'*16))
    decryptor = cipher.decryptor()
    buf = bytearray(256)
    len_encrypted = decryptor.update_into(encryptedMasterSecret, buf)
    decrypted_master_secret = buf[:len_encrypted]
    print('decrypted_master_secret', hexlify(decrypted_master_secret).decode())

    # Importing same encrypted master_secret should derive same tag as reference tag
    applet.importMasterSecret(encryptedMasterSecret)
    response = applet.deriveUserKeyHmacSecret(derivation_data_user_key)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert user_encryption_key == response.body
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert tag == response.body

    # After deleting Encryption Key, master_secret should be regenerated and derive different tag
    response = applet.deleteEncryptionKey()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert len(response) == 0
    response = applet.deriveUserKeyHmacSecret(derivation_data_user_key)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert user_encryption_key != response.body
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert tag != response.body

    # Tag should be same as reference tag after deleting encryption key and imporing decrypted master_secret
    response = applet.deleteEncryptionKey()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert len(response) == 0
    applet.importMasterSecret(decrypted_master_secret, allowExport=True)
    response = applet.deriveUserKeyHmacSecret(derivation_data_user_key)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert user_encryption_key == response.body
    response = applet.ctapMakeAuthTag(derivation_data)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert tag == response.body

    response = applet.exportEntropy()
    assert len(response) == ENTROPY_LENGTH
    assert_sw(response, ISO7816.SW_NO_ERROR)

    entropy = response.body
    master = entropy[0:32]
    master2 = entropy[32:64]
    transport = entropy[64:96]

    print("master", hexlify(master))
    print("master2", hexlify(master2))
    print("transport", hexlify(transport))

    assert hexlify(master) == hexlify(hmac.new(sha256(decrypted_master_secret), DERIVATION_CONSTANT_FIDO_MASTER, hashlib.sha256).digest())
    assert hexlify(master2) == hexlify(hmac.new(sha256(decrypted_master_secret), DERIVATION_CONSTANT_FIDO_MASTER2, hashlib.sha256).digest())
    assert hexlify(transport) == hexlify(hmac.new(sha256(decrypted_master_secret), DERIVATION_CONSTANT_FIDO_TRANSPORT, hashlib.sha256).digest())
    
    derivation_bytes = ''.join(random.choices(string.ascii_uppercase + string.digits, k=15)).encode()
    user_encryption_key = derive_user_encryption_key(
        decrypted_master_secret,
        [
            DERIVATION_CONSTANT_ENCRYPTION_KEYS,
            b'\x01', # wallet id
            DERIVATION_CONSTANT_USER_KEY_HMACSECRET, 
            derivation_bytes
        ]
    )
    response = applet.deriveUserKeyHmacSecret(derivation_bytes)
    assert hexlify(response.body) == hexlify(user_encryption_key)


def test_generate_master_secret(applet: FidoApplet):
    # Fiddling with the parameters
    response = applet.generateMasterSecret()
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)
    
    response = applet.generateMasterSecret(allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)

    entropies = set()
    
    response = applet.generateMasterSecret(exportNow=True)
    assert len(response) == MASTER_SECRET_MAX_LENGTH
    entropies.add(str(response))
    assert_sw(response, ISO7816.SW_NO_ERROR)

    response = applet.generateMasterSecret(allowExport=True, exportNow=True)
    assert len(response) == MASTER_SECRET_MAX_LENGTH
    entropies.add(str(response))
    assert_sw(response, ISO7816.SW_NO_ERROR)

    # Entropies are unique
    assert len(entropies) == 2

    response = applet.generateMasterSecret(allowExport=True, length=MASTER_SECRET_MIN_LENGTH-1)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_CONDITIONS_NOT_SATISFIED)

    response = applet.generateMasterSecret(allowExport=True, length=MASTER_SECRET_MAX_LENGTH+1)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_CONDITIONS_NOT_SATISFIED)

    for master_secret_length in range(MASTER_SECRET_MIN_LENGTH, MASTER_SECRET_MAX_LENGTH, 3):
        response = applet.generateMasterSecret(allowExport=True, length=master_secret_length)
        assert len(response) == 0
        assert_sw(response, ISO7816.SW_NO_ERROR)
        response = applet.exportMasterSecret()
        assert len(response) == master_secret_length
        assert_sw(response, ISO7816.SW_NO_ERROR)

def test_import_master_secret(applet: FidoApplet):
    master_secret = os.urandom(MASTER_SECRET_MAX_LENGTH)
    response = applet.importMasterSecret(master_secret)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)
    
    master_secret = os.urandom(MASTER_SECRET_MAX_LENGTH)
    response = applet.importMasterSecret(master_secret, allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_NO_ERROR)

    master_secret = os.urandom(0)
    response = applet.importMasterSecret(master_secret, allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)

    master_secret = os.urandom(MASTER_SECRET_MAX_LENGTH + 1)
    response = applet.importMasterSecret(master_secret, allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)

    master_secret = os.urandom(MASTER_SECRET_MIN_LENGTH - 1)
    response = applet.importMasterSecret(master_secret, allowExport=True)
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_WRONG_LENGTH)

    for master_secret_length in range(MASTER_SECRET_MIN_LENGTH, MASTER_SECRET_MAX_LENGTH, 3):
        master_secret = os.urandom(master_secret_length)
        response = applet.importMasterSecret(master_secret, allowExport=True)
        assert len(response) == 0
        assert_sw(response, ISO7816.SW_NO_ERROR)
        response = applet.exportMasterSecret()
        assert len(response) == master_secret_length
        assert master_secret == response.body
        assert_sw(response, ISO7816.SW_NO_ERROR)


def test_export_master_secret(applet: FidoApplet):
    master_secret = os.urandom(MASTER_SECRET_MAX_LENGTH)
    applet.importMasterSecret(master_secret)
    response = applet.exportMasterSecret()
    assert len(response) == 0
    assert_sw(response, ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED)
    
    master_secret = os.urandom(MASTER_SECRET_MAX_LENGTH)
    applet.importMasterSecret(master_secret, allowExport=True)
    response = applet.exportMasterSecret()
    assert len(response) == MASTER_SECRET_MAX_LENGTH
    assert response.body == master_secret
    assert_sw(response, ISO7816.SW_NO_ERROR)

    response = applet.exportEntropy()
    assert len(response) == ENTROPY_LENGTH
    entropy = response.body
    assert_sw(response, ISO7816.SW_NO_ERROR)

    for master_secret_length in range(MASTER_SECRET_MIN_LENGTH, MASTER_SECRET_MAX_LENGTH, 3):
        master_secret = os.urandom(master_secret_length)
        applet.importMasterSecret(master_secret, allowExport=True)
        response = applet.exportMasterSecret()
        assert len(response) == master_secret_length
        assert response.body == master_secret
        assert_sw(response, ISO7816.SW_NO_ERROR)

        
        response = applet.exportEntropy()
        assert len(response) == ENTROPY_LENGTH
        assert response.body != entropy
        assert_sw(response, ISO7816.SW_NO_ERROR)

        entropy = response.body
        master = entropy[0:32]
        master2 = entropy[32:64]
        transport = entropy[64:96]

        print("master", hexlify(master))
        print("master2", hexlify(master2))
        print("transport", hexlify(transport))

        assert hexlify(master) == hexlify(hmac.new(sha256(master_secret), DERIVATION_CONSTANT_FIDO_MASTER, hashlib.sha256).digest())
        assert hexlify(master2) == hexlify(hmac.new(sha256(master_secret), DERIVATION_CONSTANT_FIDO_MASTER2, hashlib.sha256).digest())
        assert hexlify(transport) == hexlify(hmac.new(sha256(master_secret), DERIVATION_CONSTANT_FIDO_TRANSPORT, hashlib.sha256).digest())
    