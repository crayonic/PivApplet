import smartcard
from fido_applet import FidoApplet
from binascii import hexlify, unhexlify
import sys
from time import sleep, time
from pprint import pprint

import cbor2

from smartcard.CardConnectionObserver import CardConnectionObserver
from smartcard.util import toHexString, toBytes

def assert_sw(apduResponse, expected_sw):
    assert apduResponse.sw == expected_sw, f"got {apduResponse.sw:02x}, expected {expected_sw:02x}"

class ISO7816:
    SW_NO_ERROR = 0x9000
    SW_UNKNOWN = 0x6F00
    SW_WRONG_LENGTH = 0x6700
    SW_INCORRECT_P1P2 = 0x6A86
    SW_WRONG_P1P2 = 0x6B00
    SW_INS_NOT_SUPPORTED = 0x6D00
    SW_SECURITY_STATUS_NOT_SATISFIED = 0x6982

class ConsoleCardConnectionObserver( CardConnectionObserver ):
    def update( self, cardconnection, ccevent ):

        if 'connect'==ccevent.type:
            print('connecting to ' + cardconnection.getReader())

        elif 'disconnect'==ccevent.type:
            print('disconnecting from ' + cardconnection.getReader())

        elif 'command'==ccevent.type:
            print('> ', toHexString( ccevent.args[0] ))

        elif 'response'==ccevent.type:
            if []==ccevent.args[0]:
                print('< [] ', "%-2X %-2X" % tuple(ccevent.args[-2:]))
            else:
                print('< ', toHexString(ccevent.args[0]), "%-2X %-2X" % tuple(ccevent.args[-2:]))

observer = ConsoleCardConnectionObserver()

def test():
    applet = None
    while not applet:
        sleep(1)
        readers = smartcard.System.readers()
        print(readers)
        for reader in readers:
            if True:
            # if "HID" in str(reader) or "PICC" in str(reader) or "rf" in str(reader):
                try:
                    connection = reader.createConnection()
                    connection.addObserver( observer )
                    connection.connect()
                    atr = connection.getATR()
                    print(reader, hexlify(bytes(atr)).decode().upper())

                    test_fido_operation(connection)
                except Exception as e:
                    print(e)
                
                continue
                SELECT_PIV = [
                    0x00, 0xA4, 0x04, 0x00, 0x0B, 
                    0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00, 0x01, 0x00]

                data, *sw = connection.transmit(SELECT_PIV)
                assert sw == [0x90, 0x00]
                applet = FidoApplet(connection, debug=True)
                response = applet.authPin('123456')
                assert_sw(response, ISO7816.SW_NO_ERROR)

    
    response = applet.generateRandom(64)
    assert_sw(response, ISO7816.SW_NO_ERROR)
    connection.disconnect()
    return

    sleep_time = 0.001
    while True:
        print(f"Sleeping {sleep_time:.3} s")
        sleep(sleep_time)
        sleep_time *= 1.1
        response = applet.generateRandom(64)
        assert_sw(response, ISO7816.SW_NO_ERROR)

def test_fido_operation(connection):
    def transmit(request_bytes, expected_sw=None):
        SELECT_FIDO = list(unhexlify(request_bytes))
        data, *sw = connection.transmit(SELECT_FIDO)
        if expected_sw is not None:
            assert sw == list(unhexlify(expected_sw))
        return data, sw


    # makeCredential
    pprint(cbor2.loads(unhexlify("A5015820C939EBD8DBA1924A3695A8F27044B1C5B143837B9974750E462CDD6B9317440902A26269646B776562617574686E2E696F646E616D656B776562617574686E2E696F03A36269644AFB9D0F00000000000000646E616D65696665777166656665776B646973706C61794E616D6569666577716665666577048AA263616C672664747970656A7075626C69632D6B6579A263616C67382264747970656A7075626C69632D6B6579A263616C67382364747970656A7075626C69632D6B6579A263616C6739010064747970656A7075626C69632D6B6579A263616C6739010164747970656A7075626C69632D6B6579A263616C6739010264747970656A7075626C69632D6B6579A263616C67382464747970656A7075626C69632D6B6579A263616C67382564747970656A7075626C69632D6B6579A263616C67382664747970656A7075626C69632D6B6579A263616C672764747970656A7075626C69632D6B657907A1627576F400")))

    transmit("00A4040008A0000006472F000100", "9000")
    transmit("80100000010400", "9000")
    frame, sw = transmit("90100000F001A5015820C939EBD8DBA1924A3695A8F27044B1C5B143837B9974750E462CDD6B9317440902A26269646B776562617574686E2E696F646E616D656B776562617574686E2E696F03A36269644AFB9D0F00000000000000646E616D65696665777166656665776B646973706C61794E616D6569666577716665666577048AA263616C672664747970656A7075626C69632D6B6579A263616C67382264747970656A7075626C69632D6B6579A263616C67382364747970656A7075626C69632D6B6579A263616C6739010064747970656A7075626C69632D6B6579A263616C6739010164747970656A7075626C69632D6B")
    data = frame
    frame, sw = transmit("801000007B6579A263616C6739010264747970656A7075626C69632D6B6579A263616C67382464747970656A7075626C69632D6B6579A263616C67382564747970656A7075626C69632D6B6579A263616C67382664747970656A7075626C69632D6B6579A263616C672764747970656A7075626C69632D6B657907A1627576F400")
    data += frame
    while sw[0] == 0x61:
        frame, sw = transmit(f"80C00000{sw[1]:02x}")
        data += frame
    print("data", len(data), hexlify(bytes(data)))
    pprint(cbor2.loads(bytes(data[1:])))

    # getAssertion ?
    # request = "01A5015820C939EBD8DBA1924A3695A8F27044B1C5B143837B9974750E462CDD6B9317440902A26269646B776562617574686E2E696F646E616D656B776562617574686E2E696F03A36269644AFB9D0F00000000000000646E616D65696665777166656665776B646973706C61794E616D6569666577716665666577048AA263616C672664747970656A7075626C69632D6B6579A263616C67382264747970656A7075626C69632D6B6579A263616C67382364747970656A7075626C69632D6B6579A263616C6739010064747970656A7075626C69632D6B6579A263616C6739010164747970656A7075626C69632D6B6579A263616C6739010264747970656A7075626C69632D6B6579A263616C67382464747970656A7075626C69632D6B6579A263616C67382564747970656A7075626C69632D6B6579A263616C67382664747970656A7075626C69632D6B6579A263616C672764747970656A7075626C69632D6B657907A1627576F5"
    # pprint(cbor2.loads(bytes(request[1:])))
    


if __name__ == "__main__":
    # test()
    # exit()
    while True:
        sleep(3)
        try:
            test()
        except Exception as e:
            print(e)
