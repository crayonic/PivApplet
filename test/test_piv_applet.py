import pytest
import smartcard
import os
import hmac
import hashlib

from binascii import hexlify, unhexlify

import cryptography
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec, utils, padding
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, load_der_public_key
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from fido_applet import PivApplet

backend = default_backend()
curve = ec.SECP256R1()

class ISO7816:
    SW_NO_ERROR = 0x9000
    SW_UNKNOWN = 0x6F00
    SW_WRONG_LENGTH = 0x6700
    SW_INCORRECT_P1P2 = 0x6A86
    SW_WRONG_P1P2 = 0x6B00
    SW_INS_NOT_SUPPORTED = 0x6D00
    SW_SECURITY_STATUS_NOT_SATISFIED = 0x6982

def assert_sw(apduResponse, expected_sw):
    assert apduResponse.sw == expected_sw, f"got {apduResponse.sw:02x}, expected {expected_sw:02x}"

@pytest.fixture(scope="session")
def unauthorized_applet():
    readers = smartcard.System.readers()
    print(readers)
    for reader in readers:
        if "Crayonic KeyVault" in str(reader):
            connection = reader.createConnection()
            connection.connect()
            atr = connection.getATR()
            SELECT_PIV = [
                0x00, 0xA4, 0x04, 0x00, 0x0B, 
                0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00, 0x01, 0x00]
            data, *sw = connection.transmit(SELECT_PIV)
            assert sw == [0x90, 0x00]
            print(reader, hexlify(bytes(atr)).decode().upper())
            return PivApplet(connection, debug=True)
    else:
        assert False

@pytest.fixture(scope="session")
def applet(unauthorized_applet: PivApplet):
    
    response = unauthorized_applet.authPin('123456')
    assert_sw(response, ISO7816.SW_NO_ERROR)

    return unauthorized_applet

def test_read_cert(applet: PivApplet):
    # response = applet.getData(PivApplet.TAG_CERT_9A)
    # print("CERT 9A:", hexlify(response.body))

    # response = applet.getData(PivApplet.TAG_CERT_9C)
    # print("CERT 9C:", hexlify(response.body))

    response = applet.getData(PivApplet.TAG_CERT_9D)
    print("CERT 9D:", hexlify(response.body))

    cert_bytes = response.body[8:-5]
    # print("CERT 9D bytes:", hexlify(cert_bytes))

    cert = cryptography.x509.load_der_x509_certificate(cert_bytes)
    public_key = cert.public_key()
    print(public_key)

    message = 32*b'\xAA'
    print("message", hexlify(message))

    encrypted_message = public_key.encrypt(message, padding.PKCS1v15())
    print("encrypted_message", len(encrypted_message), hexlify(encrypted_message))
        
    response = applet.genAuth(
        PivApplet.PIV_ALG_RSA2048, 
        0x9D, 
        unhexlify("7c820106820081820100") + encrypted_message
    )
    print("Decrypted", hexlify(response.body))

    