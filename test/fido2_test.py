import fido2.hid
from fido2.ctap2 import Ctap2
from fido2.hid import CTAPHID
from fido2 import cbor

from binascii import hexlify, unhexlify
from time import sleep
from pprint import pprint

import sys
import logging
logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')
# logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
logging.getLogger().setLevel('DEBUG')

kv_hid = next(fido2.hid.list_devices())
print(kv_hid)
# print(kv_hid.ping())

ctap = Ctap2(kv_hid)

pprint(ctap.reset())
pprint(ctap.get_info())

try:
    print("MAKE_CREDENTIAL")
    result = ctap.make_credential(
        # client_data_hash
        b'\xc99\xeb\xd8\xdb\xa1\x92J6\x95\xa8\xf2pD\xb1\xc5\xb1C\x83{\x99tu\x0eF,\xddk\x93\x17D\t',
        # rp
        {
            'id': 'webauthn.io', 
            'name': 'webauthn.io'
        },
        # user
        {
            'displayName': 'fewqfefew',
            'id': b'\xfb\x9d\x0f\x00\x00\x00\x00\x00\x00\x00',
            'name': 'fewqfefew'
        },
        [{'alg': -7, 'type': 'public-key'}, {'alg': -35, 'type': 'public-key'}, {'alg': -36, 'type': 'public-key'}, {'alg': -257, 'type': 'public-key'}, {'alg': -258, 'type': 'public-key'}, {'alg': -259, 'type': 'public-key'}, {'alg': -37, 'type': 'public-key'}, {'alg': -38, 'type': 'public-key'}, {'alg': -39, 'type': 'public-key'}, {'alg': -8, 'type': 'public-key'}],
        options={
            'uv': True
        },
        cancel_after_send=0 # Cancel after 1 keepalive
    )
except Exception as e:
    print(f"Error MAKE_CREDENTIAL {e}")
kv_hid.call(CTAPHID.CANCEL, read_response=False)

for i in range(1, 2):
    try:
        # kv_hid.call(CTAPHID.CANCEL)
        print(f"GET_ASSERTION {i}")
        result = ctap.get_assertion(
            # rp_id
            'webauthn.io',
            # client_data_hash
            b'\xd2\xd3aW|\xacD\xe7\xa6\x1b\xb1\xbb\xf6Wg\xd62\x0c\xf5w)u\x1b\xc3\xa5\xe4\xf4.\xed\xdfv^',
            allow_list=[{
                'id': b'KV\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x98aK`\xc1\xba\xc6^#\x86 k\xeb\xef=\xd0\x04Ut\xa6\xea\x92\x13\xc9\x9c/t\xb2$\x92\xb3 \xcf@&*\x94\xc1\xa9P\xa09\x7f)%\x0b`\x84\x1e\xf08\x00\x00\x00',
                'type': 'public-key'
            }],
            options={
                'uv': True
            },
            cancel_after_send=5 # Cancel after 5 keepalive
        )
    except Exception as e:
        print(f"Error while GET_ASSERTION {e}")
    kv_hid.call(CTAPHID.CANCEL, read_response=False)
    pprint(result)
# kv_hid.call(CTAPHID.CANCEL)


