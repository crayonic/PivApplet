from typing import List
from binascii import hexlify, unhexlify

import time

class ApduRequest:
    def __init__(self, class_, instruction, p1, p2, data=None):
        self.class_ = class_
        self.instruction = instruction
        self.p1 = p1
        self.p2 = p2
        if data is None:
            data = []
        if isinstance(data, bytes):
            data = list(data)
        self.data = data

    def __repr__(self):
        return f"cla={self.class_:02x}, ins={self.instruction:02x}, p1={self.p1:02x}, p1={self.p2:02x}, cd[{len(self.data):02x}]={hexlify(bytes(self.data)).decode()}"

    def to_sequence(self):
        return [
            self.class_,
            self.instruction,
            self.p1,
            self.p2,
            len(self.data),
            *self.data
        ]

    def to_bytes(self):
        return bytes(self.to_sequence())

class ApduResponse:
    def __init__(self, data: List[int], sw1: int, sw2: int):
        self.data = data
        self.sw1 = sw1
        self.sw2 = sw2

    def __str__(self):
        return hexlify(bytes(self.body) + bytes([self.sw1, self.sw2])).decode('utf8')

    def __repr__(self):
        return (
            (f"[{len(self.body)}]".encode() if self.body else b"") 
            + (hexlify(bytes(self.body)) + (b" " if self.data else b"") 
            + hexlify(bytes([self.sw1, self.sw2])))
        ).decode('utf8')

    def __len__(self):
        return len(self.data)

    @property
    def body(self):
        return bytes(self.data)

    @property
    def sw(self):
        return self.sw1*0x100 + self.sw2

class Applet:
    def __init__(self, connection, debug=False):
        self.debug = debug
        self.connection = connection

    def transmit(self, apduRequest, handleRequestChaining=True, handleResponseChaining=True):
        if self.debug:
            start = time.time()
        if handleRequestChaining and len(apduRequest.data) > 0xFF:
            for i in range(0, len(apduRequest.data), 0xFF):
                chunk = apduRequest.data[i:i + 0xFF]
                is_last_chunk = i + 0xFF >= len(apduRequest.data)
                chunkRequest = ApduRequest(
                    # apduRequest.class_ & (0x00 if is_last_chunk else 0x10),
                    apduRequest.class_ | (0x00 if is_last_chunk else 0x10),
                    apduRequest.instruction,
                    apduRequest.p1,
                    apduRequest.p2,
                    chunk
                )
                if self.debug:
                    print(">>-", repr(chunkRequest))
                apduResponse = ApduResponse(*self.connection.transmit(chunkRequest.to_sequence()))
                if self.debug:
                    print("<<-", repr(apduResponse))
        else:    
            if self.debug:
                print(">>>", repr(apduRequest))
            apduResponse = ApduResponse(*self.connection.transmit(apduRequest.to_sequence()))
        if handleResponseChaining:
            response_chain = []
            response_chain.extend(apduResponse.body)
            if self.debug:
                print("<<-", repr(apduResponse))
            while apduResponse.sw1 == 0x61:
                apduResponse = ApduResponse(*self.connection.transmit(ApduRequest(0x00, 0xc0, 0x00, 0x00).to_sequence()))
                if self.debug:
                    print("<<-", repr(apduResponse))
                response_chain.extend(apduResponse.body)
            apduResponse = ApduResponse(response_chain, apduResponse.sw1, apduResponse.sw2)
        if self.debug:
            print("<<<", repr(apduResponse), f"({(time.time()-start)*1000:0.0f} ms)")
        return apduResponse

class PivApplet(Applet):

    INS_VERIFY = 0x20
    INS_CHANGE_PIN = 0x24
    INS_RESET_PIN = 0x2C
    INS_GEN_AUTH = 0x87
    INS_GET_DATA = 0xCB
    INS_PUT_DATA = 0xDB
    INS_GEN_ASYM = 0x47
    INS_GET_RESPONSE = 0xC0

    INS_SET_MGMT = 0xff
    INS_IMPORT_ASYM = 0xfe
    INS_GET_VER = 0xfd
    INS_RESET = 0xfb
    INS_SET_PIN_RETRIES = 0xfa
    INS_ATTEST = 0xf9
    INS_GET_SERIAL = 0xf8
    INS_GET_MDATA = 0xf7

    TAG_CERT_9E = 0x01
    TAG_CHUID = 0x02
    TAG_FINGERPRINTS = 0x03
    TAG_CERT_9A = 0x05
    TAG_SECOBJ = 0x06
    TAG_CARDCAP = 0x07
    TAG_FACE = 0x08
    TAG_PRINTED_INFO = 0x09
    TAG_CERT_9C = 0x0A
    TAG_CERT_9D = 0x0B
    TAG_KEYHIST = 0x0C
    TAG_CERT_82 = 0x0D
    TAG_CERT_8C = 0x17

    PIV_ALG_DEFAULT = 0x00
    PIV_ALG_3DES = 0x03
    PIV_ALG_RSA1024 = 0x06
    PIV_ALG_RSA2048 = 0x07
    PIV_ALG_AES128 = 0x08
    PIV_ALG_AES192 = 0x0A
    PIV_ALG_AES256 = 0x0C
    PIV_ALG_ECCP256 = 0x11
    PIV_ALG_ECCP384 = 0x14

    PIV_ALG_ECCP256_SHA1 = 0xf0
    PIV_ALG_ECCP256_SHA256 = 0xf1
    PIV_ALG_ECCP384_SHA1 = 0xf2
    PIV_ALG_ECCP384_SHA256 = 0xf3
    PIV_ALG_ECCP384_SHA384 = 0xf4

    PIN_MAX_LENGTH = 8
    PUK_MAX_LENGTH = 8

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def authPin(self, pin: str):
        return self.transmit(ApduRequest(
            0x00, self.INS_VERIFY,
            0x00,
            0x80,
            (pin.encode() + b'\xFF'*self.PIN_MAX_LENGTH)[:self.PIN_MAX_LENGTH]
        ))

    def getPinRetries(self):
        return self.transmit(ApduRequest(
            0x00, self.INS_VERIFY,
            0x00,
            0x80
        ))

    def unblockPin(self, puk: str, newPin: str):
        return self.transmit(ApduRequest(
            0x00, self.INS_RESET_PIN,
            0x00,
            0x80,
            (puk.encode() + b'\xFF'*self.PUK_MAX_LENGTH)[:self.PUK_MAX_LENGTH]
            + (newPin.encode() + b'\xFF'*self.PIN_MAX_LENGTH)[:self.PIN_MAX_LENGTH]
        ))
    
    def changePin(self, pin:str, newPin: str):
        return self.transmit(ApduRequest(
            0x00, self.INS_CHANGE_PIN,
            0x00,
            0x80,
            (pin.encode() + b'\xFF'*self.PIN_MAX_LENGTH)[:self.PIN_MAX_LENGTH]
            + (newPin.encode() + b'\xFF'*self.PIN_MAX_LENGTH)[:self.PIN_MAX_LENGTH]
        ))

    def reset(self):
        return self.transmit(ApduRequest(
            0x00, self.INS_RESET,
            0x00,
            0x00,
        ))

    def getData(self, tag_id):
        return self.transmit(ApduRequest(
            0x00, self.INS_GET_DATA,
            0x3F, # magic constant
            0xFF, # magic constant
            [0x5C, 0x03, 0x5F, 0xC1, tag_id]
        ))

    def genAuth(self, algorithm, slot, data):
        return self.transmit(ApduRequest(
            0x00, self.INS_GEN_AUTH,
            algorithm,
            slot,
            data
        ))

    def getVersion(self):
        return self.transmit(ApduRequest(
            0x00, self.INS_GET_VER,
            0x00,
            0x00,
        ))

    def getSerial(self):
        return self.transmit(ApduRequest(
            0x00, self.INS_GET_SERIAL,
            0x00,
            0x00,
        ))

    def getMgmtChallenge(self):
        return self.transmit(ApduRequest(
            0x00, self.INS_GEN_AUTH,
            0x03, # YKPIV_ALGO_3DES; /* triple des */
            0x9B, # YKPIV_KEY_CARDMGM; /* management key */
            [0x7C, 0x02, 0x80, 0x00]
        ))


class FidoAppletUserKey:

    PURPOSE_HMACSECRET = 0x00
    OPERATION_HMACSECRET_SHARE = 0x00

    PURPOSE_AES256 = 0x01
    OPERATION_AES256NOPAD_ENCRYPT = 0x01
    OPERATION_AES256NOPAD_DECRYPT = 0x02

class FidoApplet(PivApplet):

    INS_FIDO_GENERATE_ENTROPY = 0x01
    INS_FIDO_IMPORT_ENTROPY = 0x02
    INS_FIDO_EXPORT_ENTROPY = 0x03
    INS_FIDO_CTAP_MAKE_AUTH_TAG = 0x04
    INS_FIDO_U2F_MAKE_AUTH_TAG = 0x05
    INS_FIDO_DERIVE_KEY_AND_SIGN = 0x06
    INS_FIDO_CTAP_NEW_KEY = 0x07
    INS_FIDO_U2F_NEW_KEY = 0x08
    INS_FIDO_GENERATE_CREDENTIAL_RANDOM = 0x09
    INS_FIDO_GET_CREDENTIAL_MASK = 0x0a
    INS_FIDO_INJECT_ATTESTATION_KEYPAIR = 0x0b
    INS_FIDO_SIGN_WITH_ATTESTATION_KEY = 0x0c
    INS_FIDO_GENERATE_RANDOM = 0x0d
    INS_FIDO_DERIVE_PUBLIC_KEY = 0x0e
    INS_FIDO_IMPORT_ENTROPY_ENCRYPTION_KEY = 0x0f
    INS_DERIVE_USER_ENCRYPTION_KEY = 0x10
    INS_GENERATE_MASTER_SECRET = 0x11
    INS_IMPORT_MASTER_SECRET = 0x12
    INS_EXPORT_MASTER_SECRET = 0x13

    

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generateEntropy(self, allowExport: bool=False, exportNow: bool=False):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_GENERATE_ENTROPY,
            0x01 if allowExport else 0x00,
            0x01 if exportNow else 0x00,
            []
        ))

    def importEntropy(self, entropy, allowExport: bool=False):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_IMPORT_ENTROPY,
            0x01 if allowExport else 0x00,
            0x00,
            entropy
        ))

    def exportEntropy(self):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_EXPORT_ENTROPY, 0x00, 0x00
        ))

    
    def ctapMakeAuthTag(self, derivation_data):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_CTAP_MAKE_AUTH_TAG, 0x00, 0x00, derivation_data
        ))
    
    def u2fMakeAuthTag(self, derivation_data):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_U2F_MAKE_AUTH_TAG, 0x00, 0x00, derivation_data
        ))

    def deriveKeyAndSign(self, hash, derivation_data):
        assert len(hash) == 32, "Invalid size of hash"
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_DERIVE_KEY_AND_SIGN, 0x00, 0x00, hash + derivation_data
        ))
    
    def derivePublicKey(self, derivation_data):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_DERIVE_PUBLIC_KEY, 0x00, 0x00, derivation_data
        ))
    
    def u2fNewKey(self, derivation_data):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_U2F_NEW_KEY, 0x00, 0x00, derivation_data
        ))
    
    def generateCredentialRandom(self, derivation_data):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_GENERATE_CREDENTIAL_RANDOM, 0x00, 0x00, derivation_data
        ))
    def getCredentialMask(self, derivation_data):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_GET_CREDENTIAL_MASK, 0x00, 0x00, derivation_data
        ))

    def injectAttestationKeypair(self, privateKey):
        # assert len(privateKey) == 32, "Invalid size private Key"
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_INJECT_ATTESTATION_KEYPAIR, 0x00, 0x00, privateKey
        ))

    def signWithAttestationKey(self, hash):
        # assert len(hash) == 32, "Invalid size of hash"
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_SIGN_WITH_ATTESTATION_KEY, 0x00, 0x00, hash
        ))

    def generateRandom(self, length):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_GENERATE_RANDOM, 0x00, length
        ))

    def importEntropyEncryptionKey(self, encryption_key):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_IMPORT_ENTROPY_ENCRYPTION_KEY, 0x00, 0x00, encryption_key
        ))

    def deleteEncryptionKey(self):
        return self.transmit(ApduRequest(
            0x00, self.INS_FIDO_IMPORT_ENTROPY_ENCRYPTION_KEY, 0x01, 0x00
        ))

    def deriveUserKey(self, purpose, operation, payload):
        return self.transmit(ApduRequest(
            0x00, self.INS_DERIVE_USER_ENCRYPTION_KEY, 
            purpose,  
            operation, 
            payload
        ))

    def deriveUserKeyHmacSecret(self, 
        derivation_bytes, 
        operation=FidoAppletUserKey.OPERATION_HMACSECRET_SHARE,
        wallet_id=0x01
    ):
        return self.deriveUserKey(
            FidoAppletUserKey.PURPOSE_HMACSECRET,  
            operation, 
            [
                0x00,
                0x00, 0x01, # wallet id length == 1
                wallet_id,
                0x01, 
                len(derivation_bytes)//256, len(derivation_bytes)%256,
                *derivation_bytes
            ]
        )
    
    def deriveUserKeyAES256(self, 
        derivation_bytes, 
        message, 
        aes_iv, 
        operation=FidoAppletUserKey.OPERATION_AES256NOPAD_ENCRYPT,
        wallet_id=0x01
    ):
        return self.deriveUserKey(
            FidoAppletUserKey.PURPOSE_AES256, 
            operation, 
            [   
                0x00,
                0x00, 0x01, # wallet id length == 1
                wallet_id, 
                0x01,
                len(derivation_bytes)//256, len(derivation_bytes)%256,
                *derivation_bytes,
                0x02, 
                len(message)//256, len(message)%256,
                *message,
                0x03, 
                len(aes_iv)//256, len(aes_iv)%256,
                *aes_iv
            ]
        )

    def generateMasterSecret(self, allowExport: bool=False, exportNow: bool=False, length=None):
        return self.transmit(ApduRequest(
            0x00, self.INS_GENERATE_MASTER_SECRET,
            0x01 if allowExport else 0x00,
            0x01 if exportNow else 0x00,
            [length] if length is not None else []
        ))

    def importMasterSecret(self, master_secret, allowExport: bool=False):
        return self.transmit(ApduRequest(
            0x00, self.INS_IMPORT_MASTER_SECRET,
            0x01 if allowExport else 0x00,
            0x00,
            master_secret
        ))

    def exportMasterSecret(self):
        return self.transmit(ApduRequest(
            0x00, self.INS_EXPORT_MASTER_SECRET, 0x00, 0x00
        ))
