import pytest
from binascii import hexlify, unhexlify
from fido_applet import FidoApplet
import os
import smartcard

class ISO7816:
    SW_NO_ERROR = 0x9000
    SW_UNKNOWN = 0x6F00
    SW_WRONG_LENGTH = 0x6700
    SW_WRONG_DATA = 0x6A80
    SW_INCORRECT_P1P2 = 0x6A86
    SW_WRONG_P1P2 = 0x6B00
    SW_INS_NOT_SUPPORTED = 0x6D00
    SW_SECURITY_STATUS_NOT_SATISFIED = 0x6982
    SW_CONDITIONS_NOT_SATISFIED = 0x6985

MASTER_SECRET_MIN_LENGTH = 16
MASTER_SECRET_MAX_LENGTH = 32
ENTROPY_LENGTH = 96
AUTH_TAG_LENGTH = 16
ENTROPY_PART_LENGTH = 32
AES_IV_LENGTH = 16
AES256_KEY_LENGTH = 32
AES_OUT_LENGTH = 32
SHA256_LENGTH = 32
PUBLIC_KEY_LENGTH = 65
PRIVATE_KEY_LENGTH = 32
KEY_HANDLE_LENGTH = 32


def assert_sw(apduResponse, expected_sw):
    assert apduResponse.sw == expected_sw, f"got {apduResponse.sw:02x}, expected {expected_sw:02x}"

def assert_not_sw(apduResponse, expected_sw):
    assert apduResponse.sw != expected_sw, f"got {apduResponse.sw:02x}, expected not be {expected_sw:02x}"

@pytest.fixture(scope="session")
def unauthorized_applet():
    readers = smartcard.System.readers()
    print(readers)
    selected_reader = None
    for reader in readers:
        if "Crayonic KeyVault" in str(reader):
            selected_reader = reader
            break
    else:
        selected_reader = readers[0]

    print("Selected reader:", selected_reader)

    connection = selected_reader.createConnection()
    connection.connect()
    atr = connection.getATR()
    print(hexlify(bytes(atr)).decode().upper())
    SELECT_PIV = [
        0x00, 0xA4, 0x04, 0x00, 0x0B, 
        0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00, 0x01, 0x00]
    data, *sw = connection.transmit(SELECT_PIV)
    assert sw == [0x90, 0x00]
    return FidoApplet(connection, debug=True)


@pytest.fixture(scope="session")
def applet(unauthorized_applet: FidoApplet):
    
    # response = unauthorized_applet.injectAttestationKeypair(os.urandom(PRIVATE_KEY_LENGTH))
    # assert_sw(response, ISO7816.SW_NO_ERROR)

    # response = unauthorized_applet.ctapMakeAuthTag(os.urandom(50))
    # assert_sw(response, ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED)

    response = unauthorized_applet.authPin('123456')
    assert_sw(response, ISO7816.SW_NO_ERROR)
    # response = unauthorized_applet.deleteEncryptionKey()
    # assert_sw(response, ISO7816.SW_NO_ERROR)

    return unauthorized_applet
