import pytest
import smartcard
import os
import hmac
import hashlib
import random
import string

from binascii import hexlify, unhexlify

import cryptography
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec, utils, padding
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, load_der_public_key
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from fido_applet import FidoApplet
from fido_applet import PivApplet

backend = default_backend()
curve = ec.SECP256R1()

from test_base import *

def test_applet_connection(unauthorized_applet: FidoApplet):
    pass

def test_reset_piv(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet

    response = applet.getPinRetries()
    if response.sw != 0x63c0: # pin retries depleted
        return

    PIN_LEN = 8
    # Deplete PINS
    for _ in range(5):
        randomPin = ''.join(random.choice(string.digits) for _ in range(PIN_LEN))
        response = applet.authPin(randomPin)
        print(response)
    
    PUK_LEN = 8
    for _ in range(3):
        randomPuk = ''.join(random.choice(string.digits) for _ in range(PUK_LEN))
        response = applet.unblockPin(randomPuk, "123456")
        print(response)

    response = applet.reset()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    assert response.body[:2] == b'\x99\x08'
    assert len(response.body[2:]) == 8
    newPuk = response.body[2:].decode()
    print("New PUK:", newPuk)
    applet.connection.getATR()


def test_piv_auth_without_pin(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet

    applet.connection.getATR()
    print("WAITING FOR SCAN FINGERPRINT, KV SHOULD ASK FOR FP")
    response = applet.authPin("")
    assert_sw(response, ISO7816.SW_NO_ERROR)


# def test_correct_pin(unauthorized_applet: FidoApplet):
#     applet = unauthorized_applet

#     applet.connection.getATR()
#     response = applet.authPin("123456")
#     assert_sw(response, ISO7816.SW_NO_ERROR)

def test_change_pin(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet

    applet.connection.getATR()
    response = applet.changePin("", "654321")
    assert_sw(response, ISO7816.SW_NO_ERROR)


def test_piv_auth_without_pin_after_change_pin(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet

    applet.connection.getATR()
    print("WAITING FOR SCAN FINGERPRINT, KV SHOULD ASK FOR FP")
    response = applet.authPin("")
    assert_sw(response, ISO7816.SW_NO_ERROR)

def test_changedpin(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet

    applet.connection.getATR()
    response = applet.authPin("654321")
    assert_sw(response, ISO7816.SW_NO_ERROR)


def test_change_pin_back_to_default(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet

    applet.connection.getATR()
    print("WAITING FOR SCAN FINGERPRINT, KV SHOULD ASK FOR FP")
    response = applet.changePin("", "123456")
    assert_sw(response, ISO7816.SW_NO_ERROR)


def test_piv_auth_like_piv_manager(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet

    applet.connection.getATR()
    # response = applet.authPin("123456")
    # assert_sw(response, ISO7816.SW_NO_ERROR)
    response = applet.getPinRetries()
    response = applet.getVersion()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    response = applet.getSerial()
    assert_sw(response, ISO7816.SW_NO_ERROR)
    response = applet.changePin("", "666666")
    assert_sw(response, ISO7816.SW_NO_ERROR)