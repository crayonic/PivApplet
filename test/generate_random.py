import smartcard
from fido_applet import FidoApplet
from binascii import hexlify, unhexlify
import sys

def assert_sw(apduResponse, expected_sw):
    assert apduResponse.sw == expected_sw, f"got {apduResponse.sw:02x}, expected {expected_sw:02x}"

class ISO7816:
    SW_NO_ERROR = 0x9000
    SW_UNKNOWN = 0x6F00
    SW_WRONG_LENGTH = 0x6700
    SW_INCORRECT_P1P2 = 0x6A86
    SW_WRONG_P1P2 = 0x6B00
    SW_INS_NOT_SUPPORTED = 0x6D00
    SW_SECURITY_STATUS_NOT_SATISFIED = 0x6982

if __name__ == "__main__":
    readers = smartcard.System.readers()
    print(readers)
    for reader in readers:
        if "ACS" in str(reader):
            connection = reader.createConnection()
            connection.connect()
            atr = connection.getATR()
            SELECT_PIV = [
                0x00, 0xA4, 0x04, 0x00, 0x0B, 
                0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00, 0x01, 0x00]
            data, *sw = connection.transmit(SELECT_PIV)
            assert sw == [0x90, 0x00]
            print(reader, hexlify(bytes(atr)).decode().upper())
            applet = FidoApplet(connection, debug=False)
            response = applet.authPin('123456')
            assert_sw(response, ISO7816.SW_NO_ERROR)

    try:
        num_bytes = int(sys.argv[1])
    except:
        num_bytes = 128

    try:
        output_filename = sys.argv[2]
    except:
        output_filename = "seed.bin"

    random_bytes = b''
    while len(random_bytes) < num_bytes:
        response = applet.generateRandom(64)
        assert_sw(response, ISO7816.SW_NO_ERROR)
        random_bytes += response.body


    with open(output_filename, 'wb') as f:
        f.write(random_bytes[:num_bytes])
    
    print(f"Written {num_bytes} bytes to {output_filename}")