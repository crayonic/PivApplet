package net.cooperi.pivapplet;

import java.security.spec.ECPrivateKeySpec;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.Util;
import javacard.security.ECPrivateKey;
import javacard.security.ECPublicKey;
import javacard.security.Signature;
import javacard.security.RandomData;
import javacard.security.KeyAgreement;
import javacard.security.KeyBuilder;
import javacard.security.KeyPair;
import javacard.security.PrivateKey;
import javacard.security.PublicKey;
import javacard.security.HMACKey;
import javacard.security.AESKey;
import javacard.security.MessageDigest;
import javacardx.crypto.Cipher;

public class Fido {

    // Future constant from 3.0.5
    private static final byte ALG_EC_SVDP_DH_PLAIN_XY = (byte)6;

    private static final byte INS_FIDO_GENERATE_ENTROPY = (byte)0x01;
	private static final byte INS_FIDO_IMPORT_ENTROPY = (byte)0x02;
	private static final byte INS_FIDO_EXPORT_ENTROPY = (byte)0x03;
	private static final byte INS_FIDO_CTAP_MAKE_AUTH_TAG = (byte)0x04;
	private static final byte INS_FIDO_U2F_MAKE_AUTH_TAG = (byte)0x05;
	private static final byte INS_FIDO_DERIVE_KEY_AND_SIGN = (byte)0x06;
	private static final byte INS_FIDO_CTAP_NEW_KEY = (byte)0x07;
	private static final byte INS_FIDO_U2F_NEW_KEY = (byte)0x08;
	private static final byte INS_FIDO_GENERATE_CREDENTIAL_RANDOM = (byte)0x09;
	private static final byte INS_FIDO_GET_CREDENTIAL_MASK = (byte)0x0a;
	private static final byte INS_FIDO_INJECT_ATTESTATION_KEYPAIR = (byte)0x0b;
	private static final byte INS_FIDO_SIGN_WITH_ATTESTATION_KEY = (byte)0x0c;
	private static final byte INS_FIDO_GENERATE_RANDOM = (byte)0x0d;
	private static final byte INS_FIDO_DERIVE_PUBLIC_KEY = (byte)0x0e;
    private static final byte INS_FIDO_IMPORT_ENTROPY_ENCRYPTION_KEY = (byte)0x0f;
    
    private static final byte INS_DERIVE_USER_ENCRYPTION_KEY = (byte)0x10;
    private static final byte INS_GENERATE_MASTER_SECRET = (byte)0x11;
    private static final byte INS_IMPORT_MASTER_SECRET = (byte)0x12;
    private static final byte INS_EXPORT_MASTER_SECRET = (byte)0x13;

    private static final short MASTER_SECRET_MIN_SIZE = 16;
    private static final short MASTER_SECRET_MAX_SIZE = 32;
    private static final short ENTROPY_SIZE = 96;
    private static final short MASTER_OFFSET = 0;
    private static final short MASTER2_OFFSET = 32;
    private static final short TRANSPORT_OFFSET = 64;
    private static final short MASTER_LENGTH = 32;
    private static final short MASTER2_LENGTH = 32;
    private static final short TRANSPORT_LENGTH = 32;

    private static final short HMAC256_OUT_SIZE = 32;
    private static final short TMP_SIZE = 64; // hmacsha256 
    private static final short CREDENTIAL_TAG_SIZE = 16;
    private static final short AES256_KEY_SIZE = 32;
    private static final short AES_BLOCK_SIZE = 16;
    private static final short EC256_PRIVATE_KEY_SIZE = 32;
    private static final short SHA256_DIGEST_SIZE = 32;
    private static final short MAGIC_NUMBER_SIZE = 2;
    private static final short KEY_HANDLE_RANDOM_SIZE = 32;

    private static final byte[] DERIVATION_CONSTANT_FIDO_MASTER = {
        'F', 'I', 'D', 'O', '_', 'M', 'A', 'S', 'T', 'E', 'R'
    };
    private static final byte[] DERIVATION_CONSTANT_FIDO_TRANSPORT = {
        'F', 'I', 'D', 'O', '_', 'T', 'R', 'A', 'N', 'S', 'P', 'O', 'R', 'T'
    };
    private static final byte[] DERIVATION_CONSTANT_FIDO_MASTER2 = {
        'F', 'I', 'D', 'O', '_', 'M', 'A', 'S', 'T', 'E', 'R', '2'
    };
    private static final byte[] DERIVATION_CONSTANT_CRYPTO_SECRET = {
        'C', 'R', 'Y', 'P', 'T', 'O', '_', 'S', 'E', 'C', 'R', 'E', 'T'
    };
    
    private byte[] masterSecret = null;
    private short masterSecretLength = 0;
    private boolean masterSecretSet = false;
    private byte[] entropy = null;
    private boolean entropySet = false;
    private byte[] tmpHmacSHA256 = null;
    private byte[] tmpAuthTag = null;
    private byte[] entropyEncryptionKey = null;
    private boolean entropyEncryptionKeySet = false;
    private byte[] tmpPrivateKeyBuffer = null;
    private byte[] tmpU2fKeyHandle = null;
    private byte[] tmpDerivationBuffer = null;
    private static final short U2F_KEY_HANDLE_SIZE =
     (short)(MAGIC_NUMBER_SIZE + CREDENTIAL_TAG_SIZE + KEY_HANDLE_RANDOM_SIZE);

    private boolean masterSecretExportEnabled = false;
    private boolean entropyExportEnabled = false;
    private MessageDigest sha256 = null;
    private Signature ecdsa = null;
    private KeyAgreement ecpublic = null;
    private RandomData randData = null;
    private Cipher aes = null;
    private ECPrivateKey attestationPrivateKey = null;
    private ECPrivateKey privateKey = null;
    private AESKey aesKey = null;

    public Fido(
        MessageDigest sha256, 
        Signature ecdsa,
        RandomData randData,
        Cipher aes)
    {
        this.sha256 = sha256;
        this.ecdsa = ecdsa;
        final byte ALG_EC_SVDP_DH_PLAIN_XY = 6; // constant from JavaCard 3.0.5
        this.ecpublic = KeyAgreement.getInstance(ALG_EC_SVDP_DH_PLAIN_XY, false);
        this.randData = randData;
        this.aes = aes;
        this.masterSecret = new byte[MASTER_SECRET_MAX_SIZE];
        this.entropy = new byte[ENTROPY_SIZE];
        this.entropyEncryptionKey = new byte[AES256_KEY_SIZE];
        this.tmpPrivateKeyBuffer = JCSystem.makeTransientByteArray(TMP_SIZE, JCSystem.CLEAR_ON_DESELECT);
        this.tmpU2fKeyHandle = JCSystem.makeTransientByteArray(TMP_SIZE, JCSystem.CLEAR_ON_DESELECT);
        this.tmpHmacSHA256 = JCSystem.makeTransientByteArray(TMP_SIZE, JCSystem.CLEAR_ON_DESELECT);
        this.tmpAuthTag = JCSystem.makeTransientByteArray(HMAC256_OUT_SIZE, JCSystem.CLEAR_ON_DESELECT);
        this.tmpDerivationBuffer = JCSystem.makeTransientByteArray(HMAC256_OUT_SIZE, JCSystem.CLEAR_ON_DESELECT);
        // if (true) return;
        this.attestationPrivateKey = (ECPrivateKey)KeyBuilder.buildKey(
            KeyBuilder.TYPE_EC_FP_PRIVATE, KeyBuilder.LENGTH_EC_FP_256, false
        );
        ECParams.setCurveParametersP256(this.attestationPrivateKey);
        this.privateKey = (ECPrivateKey)KeyBuilder.buildKey(
            KeyBuilder.TYPE_EC_FP_PRIVATE_TRANSIENT_DESELECT,
            KeyBuilder.LENGTH_EC_FP_256, 
            false
        );

        this.aesKey = (AESKey)KeyBuilder.buildKey(
            KeyBuilder.TYPE_AES_TRANSIENT_DESELECT, KeyBuilder.LENGTH_AES_256, false);

        this.reset();
    }

    public void reset() {
        // this.randData.generateData(this.entropy, (short)0, ENTROPY_SIZE);
        // this.entropyExportEnabled = false;
        this.randData.generateData(this.masterSecret, (short)0, MASTER_SECRET_MAX_SIZE);
        this.masterSecretLength = MASTER_SECRET_MAX_SIZE;
        this.masterSecretExportEnabled = false;
        deriveFidoEntropyFromMasterSecret();
    }

    public boolean process(APDU apdu, boolean pinValidated) {
        final byte[] buffer = apdu.getBuffer();
        final byte ins = buffer[ISO7816.OFFSET_INS];
        switch (ins) {
            case INS_FIDO_GENERATE_ENTROPY:
            case INS_FIDO_IMPORT_ENTROPY:
            case INS_FIDO_EXPORT_ENTROPY:
            case INS_FIDO_CTAP_MAKE_AUTH_TAG:
            case INS_FIDO_U2F_MAKE_AUTH_TAG:
            case INS_FIDO_DERIVE_KEY_AND_SIGN:
            case INS_FIDO_U2F_NEW_KEY:
            case INS_FIDO_GENERATE_CREDENTIAL_RANDOM:
            case INS_FIDO_GET_CREDENTIAL_MASK:
            case INS_FIDO_INJECT_ATTESTATION_KEYPAIR:
            case INS_FIDO_SIGN_WITH_ATTESTATION_KEY:
            case INS_FIDO_GENERATE_RANDOM:
            case INS_FIDO_DERIVE_PUBLIC_KEY:
            case INS_FIDO_IMPORT_ENTROPY_ENCRYPTION_KEY:
            case INS_DERIVE_USER_ENCRYPTION_KEY:
            case INS_GENERATE_MASTER_SECRET:
            case INS_IMPORT_MASTER_SECRET:
            case INS_EXPORT_MASTER_SECRET:
                apdu.setIncomingAndReceive();
                break;
            default:
                return false;
        }
        switch (ins) {
            case INS_FIDO_INJECT_ATTESTATION_KEYPAIR:
                break;
            default:
                if (!pinValidated) {
                    ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
                    return true;
                }
                break;
        }
        switch (ins) {
            case INS_FIDO_GENERATE_ENTROPY:
                processGenerateEntropy(apdu);
                break;
            case INS_FIDO_IMPORT_ENTROPY:
                processImportEntropy(apdu);
                break;
            case INS_FIDO_EXPORT_ENTROPY:
                processExportEntropy(apdu);
                break;
            case INS_FIDO_CTAP_MAKE_AUTH_TAG:
                processCtapMakeAuthTag(apdu);
                break;
            case INS_FIDO_U2F_MAKE_AUTH_TAG:
                processU2fMakeAuthTag(apdu);
                break;
            case INS_FIDO_DERIVE_KEY_AND_SIGN:
                processDeriveKeyAndSign(apdu);
                break;
            case INS_FIDO_DERIVE_PUBLIC_KEY:
                processDerivePublicKey(apdu);
                break;
            case INS_FIDO_U2F_NEW_KEY:
                processU2fNewKey(apdu);
                break;
            case INS_FIDO_GENERATE_CREDENTIAL_RANDOM:
                processGenerateCredentialRandom(apdu);
                break;
            case INS_FIDO_GET_CREDENTIAL_MASK:
                processGetCredentialMask(apdu);
                break;
            case INS_FIDO_INJECT_ATTESTATION_KEYPAIR:
                processInjectAttestationKeypair(apdu);
                break;
            case INS_FIDO_SIGN_WITH_ATTESTATION_KEY:
                processSignWithAttestationKey(apdu);
                break;
            case INS_FIDO_GENERATE_RANDOM:
                processGenerateRandom(apdu);
                break;
            case INS_FIDO_IMPORT_ENTROPY_ENCRYPTION_KEY:
                processImportEntropyEncryptionKey(apdu);
                break;
            case INS_DERIVE_USER_ENCRYPTION_KEY:
                processDeriveUserEncryptionKey(apdu);
                break;
            case INS_GENERATE_MASTER_SECRET:
                processGenerateMasterSecret(apdu);
                break;
            case INS_IMPORT_MASTER_SECRET:
                processImportMasterSecret(apdu);
                break;
            case INS_EXPORT_MASTER_SECRET:
                processExportMasterSecret(apdu);
                break;
            default:
                // This should not happen, because of the switch check above
                ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
        return true;
    }

    // Copies entropy to buffer, encrypted if encryption key is set
    private short copyEntropyToBuffer(byte[] outBuff, short outOffset) {
        if (this.entropyEncryptionKeySet) {
            this.aesKey.setKey(this.entropyEncryptionKey, (short)0);
            this.aes.init(this.aesKey, Cipher.MODE_ENCRYPT);
            short encryptedEntropyLength = this.aes.doFinal(
                this.entropy, (short)0, ENTROPY_SIZE,
                outBuff, outOffset
            );
            return encryptedEntropyLength;
        } else {
            Util.arrayCopy(
                this.entropy, (short)0,
                outBuff, outOffset,
                ENTROPY_SIZE
            );
            return ENTROPY_SIZE;
        }
    }

    public void processGenerateEntropy(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        if (apdu.getIncomingLength() != 0) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        this.randData.generateData(this.entropy, (short)0, ENTROPY_SIZE);
        this.entropySet = true;
        switch(buffer[ISO7816.OFFSET_P1]) {
            case 0x00: // forbid export
                this.entropyExportEnabled = false;
                break;
            case 0x01: // allow future export
                this.entropyExportEnabled = true;
                break;
            default:
                ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
        }
        switch(buffer[ISO7816.OFFSET_P2]) {
            case 0x00: // generated entropy not in response
                apdu.setOutgoingAndSend((short)0, (short)0);
                break;
            case 0x01: // entropy exported in response
                final short entropyLength = this.copyEntropyToBuffer(buffer, (short)0);
                this.entropySet = true;
                apdu.setOutgoingAndSend((short)0, entropyLength);
                break;
            default:
                ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
        }
    }

    public void processImportEntropy(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        if (apdu.getIncomingLength() != ENTROPY_SIZE) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        if (this.entropyEncryptionKeySet) {
            this.aesKey.setKey(this.entropyEncryptionKey, (short)0);
            this.aes.init(this.aesKey, Cipher.MODE_DECRYPT);
            short decryptedEntropyLength = this.aes.doFinal(
                buffer, apdu.getOffsetCdata(), ENTROPY_SIZE,
                this.entropy, (short)0
            );
        } else {
            Util.arrayCopy(
                buffer, apdu.getOffsetCdata(),
                this.entropy, (short)0,
                ENTROPY_SIZE
            );
        }
        switch(buffer[ISO7816.OFFSET_P1]) {
            case 0x00: // forbid export
                this.entropyExportEnabled = false;
                break;
            case 0x01: // allow future export
                this.entropyExportEnabled = true;
                break;
            default:
                ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
        }
        this.entropySet = true;
        apdu.setOutgoingAndSend((short)0, (short)0);
    }

    public void processExportEntropy(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        if (apdu.getIncomingLength() != 0) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        if (this.entropyExportEnabled) {
            final short entropyLength = this.copyEntropyToBuffer(buffer, (short)0);
            apdu.setOutgoingAndSend((short)0, entropyLength);
        } else {
            ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
        }
    }

    
    // Copies entropy to buffer, encrypted if encryption key is set
    private short copyMasterSecretToBuffer(byte[] outBuff, short outOffset) {
        if (this.entropyEncryptionKeySet) {
            this.aesKey.setKey(this.entropyEncryptionKey, (short)0);
            this.aes.init(this.aesKey, Cipher.MODE_ENCRYPT);
            short encryptedMasterSecretLength = this.aes.doFinal(
                this.masterSecret, (short)0, this.masterSecretLength,
                outBuff, outOffset
            );
            return encryptedMasterSecretLength;
        } else {
            Util.arrayCopy(
                this.masterSecret, (short)0,
                outBuff, outOffset,
                masterSecretLength
            );
            return masterSecretLength;
        }
    }

    public void processGenerateMasterSecret(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        short requestedMasterSecretSize = MASTER_SECRET_MAX_SIZE;
        if (apdu.getIncomingLength() == 0) {
            requestedMasterSecretSize = MASTER_SECRET_MAX_SIZE;
        } else if (apdu.getIncomingLength() == 1) {
            requestedMasterSecretSize = (short)buffer[apdu.getOffsetCdata()];
        } else {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        if (requestedMasterSecretSize < MASTER_SECRET_MIN_SIZE 
            || requestedMasterSecretSize > MASTER_SECRET_MAX_SIZE) 
        {
            ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
        }
        Util.arrayFillNonAtomic(this.masterSecret, (short)0, MASTER_SECRET_MAX_SIZE, (byte)0);
        this.randData.generateData(this.masterSecret, (short)0, requestedMasterSecretSize);
        this.masterSecretLength = requestedMasterSecretSize;
        this.masterSecretSet = true;
        switch(buffer[ISO7816.OFFSET_P1]) {
            case 0x00: // forbid export
                this.masterSecretExportEnabled = false;
                this.entropyExportEnabled = false;
                break;
            case 0x01: // allow future export
                this.masterSecretExportEnabled = true;
                this.entropyExportEnabled = true;
                break;
            default:
                ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
        }
        switch(buffer[ISO7816.OFFSET_P2]) {
            case 0x00: // generated master secret not in response
                apdu.setOutgoingAndSend((short)0, (short)0);
                break;
            case 0x01: // master secret exported in response
                final short copiedMasterSecretLength = this.copyMasterSecretToBuffer(buffer, (short)0);
                apdu.setOutgoingAndSend((short)0, copiedMasterSecretLength);
                break;
            default:
                ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
        }
        this.masterSecretSet = true;
        deriveFidoEntropyFromMasterSecret();
        this.entropySet = true;
    }

    public void processImportMasterSecret(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        final short incomingMasterSecretLength = apdu.getIncomingLength();
        if (incomingMasterSecretLength < MASTER_SECRET_MIN_SIZE
            || incomingMasterSecretLength > MASTER_SECRET_MAX_SIZE) 
        {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        if (this.entropyEncryptionKeySet) {
            this.aesKey.setKey(this.entropyEncryptionKey, (short)0);
            this.aes.init(this.aesKey, Cipher.MODE_DECRYPT);
            short decryptedMasterSecretLength = this.aes.doFinal(
                buffer, apdu.getOffsetCdata(), incomingMasterSecretLength,
                this.masterSecret, (short)0
            );
            this.masterSecretLength = incomingMasterSecretLength;
        } else {
            Util.arrayCopy(
                buffer, apdu.getOffsetCdata(),
                this.masterSecret, (short)0,
                incomingMasterSecretLength
            );
            this.masterSecretLength = incomingMasterSecretLength;
        }
        switch(buffer[ISO7816.OFFSET_P1]) {
            case 0x00: // forbid export
                this.masterSecretExportEnabled = false;
                this.entropyExportEnabled = false;
                break;
            case 0x01: // allow future export
                this.masterSecretExportEnabled = true;
                this.entropyExportEnabled = true;
                break;
            default:
                ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
        }
        this.masterSecretSet = true;
        apdu.setOutgoingAndSend((short)0, (short)0);
        deriveFidoEntropyFromMasterSecret();
        this.entropySet = true;
    }

    public void processExportMasterSecret(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        if (apdu.getIncomingLength() != 0) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        if (this.masterSecretExportEnabled) {
            final short masterSecretLength = this.copyMasterSecretToBuffer(buffer, (short)0);
            apdu.setOutgoingAndSend((short)0, masterSecretLength);
        } else {
            ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
        }
    }

    public void deriveFidoEntropyFromMasterSecret() {
        this.deriveMasterNodeSha256();
        this.hmacSHA256(
            this.tmpDerivationBuffer, (short)0, SHA256_DIGEST_SIZE, 
            Fido.DERIVATION_CONSTANT_FIDO_MASTER, (short)0, (short)0, 
            Fido.DERIVATION_CONSTANT_FIDO_MASTER, (short)0, (short)(Fido.DERIVATION_CONSTANT_FIDO_MASTER.length), 
            this.entropy, MASTER_OFFSET
        );
        this.hmacSHA256(
            this.tmpDerivationBuffer, (short)0, SHA256_DIGEST_SIZE, 
            Fido.DERIVATION_CONSTANT_FIDO_TRANSPORT, (short)0, (short)0, 
            Fido.DERIVATION_CONSTANT_FIDO_TRANSPORT, (short)0, (short)(Fido.DERIVATION_CONSTANT_FIDO_TRANSPORT.length), 
            this.entropy, TRANSPORT_OFFSET
        );
        this.hmacSHA256(
            this.tmpDerivationBuffer, (short)0, SHA256_DIGEST_SIZE, 
            Fido.DERIVATION_CONSTANT_FIDO_MASTER2, (short)0, (short)0, 
            Fido.DERIVATION_CONSTANT_FIDO_MASTER2, (short)0, (short)(Fido.DERIVATION_CONSTANT_FIDO_MASTER2.length), 
            this.entropy, MASTER2_OFFSET
        );
    }

    public void processImportEntropyEncryptionKey(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        switch(buffer[ISO7816.OFFSET_P1]) {
            case 0x00: // import
                if (apdu.getIncomingLength() != AES256_KEY_SIZE) {
                    ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
                }
                this.entropyExportEnabled = false;
                this.randData.generateData(this.entropy, (short)0, ENTROPY_SIZE);
                this.entropyEncryptionKeySet = true;
                Util.arrayCopy(
                    buffer, apdu.getOffsetCdata(),
                    this.entropyEncryptionKey, (short)0,
                    AES256_KEY_SIZE
                );
                break;
            case 0x01: // delete encryption key
                if (apdu.getIncomingLength() != (short)0) {
                    ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
                }
                this.reset();
                this.entropyEncryptionKeySet = false;
                Util.arrayFillNonAtomic(this.entropyEncryptionKey, (short)0, (short)this.entropyEncryptionKey.length, (byte)0);
                break;
            default:
                ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
        }
        apdu.setOutgoingAndSend((short)0, (short)0);
    }

    public short makeAuthTag(
        byte[] keyBuffer, short keyOffset, short keyLength,
        byte[] inBuffer, short inOffset, short inLength,
        byte[] outBuffer, short outOffset
    ) {
        return this.makeAuthTagExtended(
            keyBuffer, keyOffset, keyLength,
            inBuffer, inOffset, (short)0,
            inBuffer, inOffset, inLength,
            outBuffer, outOffset
        );
    }

    public short makeAuthTagExtended(
        byte[] keyBuffer, short keyOffset, short keyLength,
        byte[] inBuffer1, short inOffset1, short inLength1,
        byte[] inBuffer2, short inOffset2, short inLength2,
        byte[] outBuffer, short outOffset
    ) {
        this.hmacSHA256(
            keyBuffer, keyOffset, keyLength, 
            inBuffer1, inOffset1, inLength1, 
            inBuffer2, inOffset2, inLength2, 
            this.tmpAuthTag, (short)0
        );
        Util.arrayCopy(
            this.tmpAuthTag, (short)0,
            outBuffer, outOffset,
            CREDENTIAL_TAG_SIZE
        );
        return CREDENTIAL_TAG_SIZE;
    }

    public short ctapMakeAuthTag(
        byte[] inBuffer, short inOffset, short inLength,
        byte[] outBuffer, short outOffset
    ) {
        return this.makeAuthTag(
            this.entropy, TRANSPORT_OFFSET, TRANSPORT_LENGTH, 
            inBuffer, inOffset, inLength, 
            outBuffer, outOffset
        );
    }

    public void processCtapMakeAuthTag(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        short tagLength = this.ctapMakeAuthTag(
            buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), 
            buffer, (short)0
        );
        apdu.setOutgoingAndSend((short)0, tagLength);
    }

    public short u2fMakeAuthTag(
        byte[] inBuffer, short inOffset, short inLength,
        byte[] outBuffer, short outOffset
    ) {
        return this.makeAuthTag(
            this.entropy, MASTER_OFFSET, MASTER_LENGTH, 
            inBuffer, inOffset, inLength, 
            outBuffer, outOffset
        );
    }

    public short u2fMakeAuthTagExtended(
        byte[] inBuffer1, short inOffset1, short inLength1,
        byte[] inBuffer2, short inOffset2, short inLength2,
        byte[] outBuffer, short outOffset
    ) {
        return this.makeAuthTagExtended(
            this.entropy, MASTER_OFFSET, MASTER_LENGTH, 
            inBuffer1, inOffset1, inLength1, 
            inBuffer2, inOffset2, inLength2, 
            outBuffer, outOffset
        );
    }

    public void processU2fMakeAuthTag(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        short tagLength = this.u2fMakeAuthTag(
            buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), 
            buffer, (short)0
        );
        apdu.setOutgoingAndSend((short)0, tagLength);
    }

    private short derivePrivateKey(
        byte[] inBuff, short inOffset, short inLength,
        byte[] outBuff, short outOffset
    ) {
        this.hmacSHA256(
            this.entropy, MASTER_OFFSET, MASTER_LENGTH, 
            inBuff, inOffset, inLength, 
            this.entropy, MASTER_OFFSET, MASTER_LENGTH, 
            outBuff, outOffset
        );
        this.aesKey.setKey(this.entropy, MASTER2_OFFSET);
        this.aes.init(this.aesKey, Cipher.MODE_ENCRYPT);
        this.aes.doFinal(
            outBuff, outOffset, EC256_PRIVATE_KEY_SIZE,
            outBuff, outOffset
        );
        return EC256_PRIVATE_KEY_SIZE;
    }

    private short ecSignHash(
        byte[] keyBuffer, short keyOffset, short keyLength,
        byte[] hashBuffer, short hashOffset, short hashLength,
        byte[] outBuffer, short outOffset
    ) {
        ECParams.setCurveParametersP256(privateKey);
        privateKey.setS(keyBuffer, keyOffset, keyLength);
        this.ecdsa.init(privateKey, Signature.MODE_SIGN);
        short signatureLength = this.ecdsa.signPreComputedHash(
            hashBuffer, hashOffset, hashLength, 
            outBuffer, outOffset
        );
        return signatureLength;
    }

    public void processDeriveKeyAndSign(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        final short hashOffset = apdu.getOffsetCdata();
        final short hashLength = SHA256_DIGEST_SIZE;
        final short messageOffset = (short)(apdu.getOffsetCdata() + hashLength);
        final short messageLength = (short)(apdu.getIncomingLength() - hashLength);

        short keyLength = this.derivePrivateKey(
            buffer, messageOffset, messageLength,
            tmpPrivateKeyBuffer, (short)0
        );

        short signatureLength = this.ecSignHash(
            tmpPrivateKeyBuffer, (short)0, keyLength,
            buffer, hashOffset, hashLength,
            buffer, (short)0
        );
        apdu.setOutgoingAndSend((short)0, signatureLength);
    }

    public void processDerivePublicKey(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        final short messageOffset = apdu.getOffsetCdata();
        final short messageLength = apdu.getIncomingLength();
        
        short keyLength = this.derivePrivateKey(
            buffer, messageOffset, messageLength,
            tmpPrivateKeyBuffer, (short)0
        );
        ECParams.setCurveParametersP256(privateKey);
        privateKey.setS(tmpPrivateKeyBuffer, (short)0, keyLength);
        final short publicKeyLength = this.derivePublicKey(
            privateKey,
            buffer, (short)0
        );
        apdu.setOutgoingAndSend((short)0, publicKeyLength);
    }

    // Derives public key by multiplying with G point on the curve
    private short derivePublicKey(
        ECPrivateKey privateKey,
        byte[] outBuffer, short outOffset
    ) {
        this.ecpublic.init(privateKey);
        return this.ecpublic.generateSecret(
            ECParams.nistp256_G, (short)0, (short)ECParams.nistp256_G.length,
            outBuffer, outOffset
        );
    }

    public void processU2fNewKey(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        final short authTagOffset = 0; // Future offset after array copy
        final short keyHandleOffset = CREDENTIAL_TAG_SIZE;
        final short appIdOffset = CREDENTIAL_TAG_SIZE + KEY_HANDLE_RANDOM_SIZE; // Future offset after array copy
        final short appIdLength = apdu.getIncomingLength();
        // check if incoming appid is enough length (==32)
        Util.arrayCopy(
            buffer, apdu.getOffsetCdata(),
            buffer, appIdOffset,
            appIdLength
        );
        this.randData.generateData(buffer, (short)0, KEY_HANDLE_RANDOM_SIZE);

        this.tmpU2fKeyHandle[0] = (byte)'K';
        this.tmpU2fKeyHandle[1] = (byte)'V';
        final short authTagLength = this.u2fMakeAuthTagExtended(
            buffer, keyHandleOffset, KEY_HANDLE_RANDOM_SIZE, 
            buffer, appIdOffset, appIdLength, 
            this.tmpU2fKeyHandle, MAGIC_NUMBER_SIZE // 2
        );
        Util.arrayCopy(
            this.tmpU2fKeyHandle, MAGIC_NUMBER_SIZE,
            buffer, authTagOffset,
            authTagLength
        );
        Util.arrayCopy(
            buffer, keyHandleOffset,
            this.tmpU2fKeyHandle, (short)(MAGIC_NUMBER_SIZE+CREDENTIAL_TAG_SIZE), // 2+16
            KEY_HANDLE_RANDOM_SIZE
        );

        final short privateKeyLength = this.derivePrivateKey(
            this.tmpU2fKeyHandle, (short)0, U2F_KEY_HANDLE_SIZE,
            tmpPrivateKeyBuffer, (short)0
        );
        ECParams.setCurveParametersP256(privateKey);
        privateKey.setS(tmpPrivateKeyBuffer, (short)0, privateKeyLength);
        final short publicKeyLength = this.derivePublicKey(
            privateKey,
            buffer, appIdOffset
        );
        apdu.setOutgoingAndSend((short)0, (short)(KEY_HANDLE_RANDOM_SIZE + authTagLength + publicKeyLength));
    }

    public void processGenerateCredentialRandom(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        // TODO check length of cdata (should be 32 + 1)
        short credentialRandomLength = this.ctapMakeAuthTag(
            buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), 
            buffer, (short)0
        );
        apdu.setOutgoingAndSend((short)0, credentialRandomLength);
    }

    public void processGetCredentialMask(APDU apdu) {
        final short CREDENTIAL_MASK_DERIVATION_SIZE = 14;
        final byte[] buffer = apdu.getBuffer();
        if (apdu.getIncomingLength() != CREDENTIAL_MASK_DERIVATION_SIZE) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        short credentialMaskLength = this.ctapMakeAuthTag(
            buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), 
            buffer, (short)0
        );
        // maybe out length should be 4, because only just uint32 is xored against first bytes
        apdu.setOutgoingAndSend((short)0, credentialMaskLength);
    }

    public void processInjectAttestationKeypair(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        if (apdu.getIncomingLength() != EC256_PRIVATE_KEY_SIZE) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        this.attestationPrivateKey.setS(buffer, apdu.getOffsetCdata(), apdu.getIncomingLength());
        short publicKeyLength = this.derivePublicKey(
            this.attestationPrivateKey,
            buffer, (short)0
        );
        apdu.setOutgoingAndSend((short)0, publicKeyLength);
    }

    public void processSignWithAttestationKey(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        if (apdu.getIncomingLength() != SHA256_DIGEST_SIZE) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        } 
        this.ecdsa.init(this.attestationPrivateKey, Signature.MODE_SIGN);
        short signatureLength = this.ecdsa.signPreComputedHash(
            buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), 
            buffer, (short)0
        );
        apdu.setOutgoingAndSend((short)0, signatureLength);
    }

    public void processGenerateRandom(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();
        final short randomLength = (short)buffer[ISO7816.OFFSET_P2];
        this.randData.generateData(buffer, (short)0, randomLength);
        apdu.setOutgoingAndSend((short)0, randomLength);
    }

    public void deriveMasterNodeSha256() {
        this.sha256.reset();
        this.sha256.doFinal(
            this.masterSecret, (short)0, this.masterSecretLength,
            this.tmpDerivationBuffer, (short)0
        );
    }

    public void processDeriveUserEncryptionKey(APDU apdu) {
        final byte[] buffer = apdu.getBuffer();

        byte purpose = buffer[ISO7816.OFFSET_P1];
        byte operation = buffer[ISO7816.OFFSET_P2];

        switch(purpose) {
            case 0x00: // HMAC-SECRET
            case 0x01: // AES-256
                break;
            default:
                ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
                return;
        }

        final short walletIdOffsetTlvStart = findTlvTag(buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), (byte)0x00);
        final short walletIdOffset = getTlvDataOffset(walletIdOffsetTlvStart);
        final short walletIdLength = getTlvLength(buffer, walletIdOffsetTlvStart);

        final short derivationKeyOffsetTlvStart = findTlvTag(buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), (byte)0x01);
        final short derivationKeyOffset = getTlvDataOffset(derivationKeyOffsetTlvStart);
        final short derivationKeyLength = getTlvLength(buffer, derivationKeyOffsetTlvStart);
        
        final short payloadOffsetTlvStart = findTlvTag(buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), (byte)0x02);
        final short payloadOffset = getTlvDataOffset(payloadOffsetTlvStart);
        final short payloadLength = getTlvLength(buffer, payloadOffsetTlvStart);

        final short payload2OffsetTlvStart = findTlvTag(buffer, apdu.getOffsetCdata(), apdu.getIncomingLength(), (byte)0x03);
        final short payload2Offset = getTlvDataOffset(payload2OffsetTlvStart);
        final short payload2Length = getTlvLength(buffer, payload2OffsetTlvStart);
        
        // 0. Derive master node from master secret
        this.deriveMasterNodeSha256();

        // 1. Derive crypto secret path
        this.hmacSHA256(
            this.tmpDerivationBuffer, (short)0, SHA256_DIGEST_SIZE, 
            Fido.DERIVATION_CONSTANT_CRYPTO_SECRET, (short)0, (short)(Fido.DERIVATION_CONSTANT_CRYPTO_SECRET.length), 
            this.tmpDerivationBuffer, (short)0
        );

        // 2. Derive Wallet ID 
        byte walletId = 0x01;
        if (walletIdOffsetTlvStart >= 0 && walletIdLength == 1) {
            walletId = buffer[walletIdOffset];
        } else {
            ISOException.throwIt(ISO7816.SW_WRONG_DATA);
            return;
        }
        final byte[] walletIdDerivationBuffer = { walletId }; 
        this.hmacSHA256(
            this.tmpDerivationBuffer, (short)0, HMAC256_OUT_SIZE, 
            walletIdDerivationBuffer, (short)0, (short)walletIdDerivationBuffer.length,
            this.tmpDerivationBuffer, (short)0
        );

        // 3. Derive purpose
        final byte[] keyPurposeDerivationBuffer = { purpose };
        this.hmacSHA256(
            this.tmpDerivationBuffer, (short)0, HMAC256_OUT_SIZE, 
            keyPurposeDerivationBuffer, (short)0, (short)keyPurposeDerivationBuffer.length,
            this.tmpDerivationBuffer, (short)0
        );

        if (derivationKeyOffsetTlvStart < 0
            || derivationKeyLength < 1
        ) {
            ISOException.throwIt(ISO7816.SW_WRONG_DATA);
            return;
        }

        // 4. Derive key leaf
        this.hmacSHA256(
            this.tmpDerivationBuffer, (short)0, HMAC256_OUT_SIZE, 
            buffer, derivationKeyOffset, derivationKeyLength,
            this.tmpDerivationBuffer, (short)0
        );

        short outLength = 0;
        
        switch(purpose) {
            case 0x00: // HMAC-SECRET
                switch(buffer[ISO7816.OFFSET_P2]) {
                    case 0x00: // Share secret
                        outLength = HMAC256_OUT_SIZE;
                        Util.arrayCopy(
                            this.tmpDerivationBuffer, (short)0,
                            buffer, (short)0, outLength
                        );
                        break;
                    default:
                        ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
                        return;
                }
                break;
            case 0x01: // AES-256
                if (payloadOffset < 0
                    || payloadLength % AES_BLOCK_SIZE != 0 
                    || payloadLength == 0 
                    || payload2Offset < 0
                    || payload2Length != AES_BLOCK_SIZE
                ) {
                    ISOException.throwIt(ISO7816.SW_WRONG_DATA);
                    return;
                }
                switch(buffer[ISO7816.OFFSET_P2]) {
                    case 0x01: // Encrypt
                        this.aesKey.setKey(this.tmpDerivationBuffer, (short)0);
                        this.aes.init(this.aesKey, Cipher.MODE_ENCRYPT, buffer, payload2Offset, payload2Length);
                        outLength = this.aes.doFinal(
                            buffer, payloadOffset, payloadLength,
                            buffer, (short)0
                        );
                        break;
                    case 0x02: // Decrypt
                        this.aesKey.setKey(this.tmpDerivationBuffer, (short)0);
                        this.aes.init(this.aesKey, Cipher.MODE_DECRYPT, buffer, payload2Offset, payload2Length);
                        outLength = this.aes.doFinal(
                            buffer, payloadOffset, payloadLength,
                            buffer, (short)0
                        );
                        break;
                    default:
                        ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
                        return;
                }
                break;
            default:
                ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
                return;
        }
        

        apdu.setOutgoingAndSend((short)0, outLength);
    }

    // Security considerations:
    // Throws when accessing outside buffer boundaries
    // When tag is found multiple times, returns -1 as error
    private short findTlvTag(
        byte[] buffer, short bufferOffset, short bufferLength,
        byte type
    ) {
        final short TLV_HEADER_LENGTH = 3; // 3 tlv header (1 type, 2 length)
        boolean found = false;
        short foundIndex = (short) -1;
        for (short i=bufferOffset; (short)(i+TLV_HEADER_LENGTH)<(short)(bufferOffset+bufferLength);) {
            if (i == (short)(bufferOffset+bufferLength)) {
                return foundIndex;
            }
            short tlvLength = getTlvLength(buffer, i);
            if (buffer[i] == type) {
                if (!found) {
                    found = true;
                    foundIndex = i;
                } else { // tag found multiple times
                    return (short) -1;
                }
            }
            i += (short)(TLV_HEADER_LENGTH + tlvLength);
            if (i > (short)(bufferOffset+bufferLength)) {
                return  (short) -1;
            }
        }
        return foundIndex;
    }

    private short getTlvLength(byte[] buffer, short bufferOffset) {
        if (bufferOffset < 0) {
            return (short) -1;
        }
        return Util.getShort(buffer, (short)(bufferOffset+1));
    }

    private short getTlvDataOffset(short bufferOffset) {
        if (bufferOffset < 0) {
            return bufferOffset;
        }
        return (short)(bufferOffset + 3);
    }

    private short hmacSHA256(
        byte[] key, short keyOff, short keyLen, 
        byte[] in, short inOff, short inLen, 
        byte[] out, short outOff
    ) {
        return this.hmacSHA256(
            key, keyOff, keyLen, 
            in, inOff, (short)0, 
            in, inOff, inLen, 
            out, outOff);
    }

    private short hmacSHA256(
        byte[] key, short keyOff, short keyLen, 
        byte[] in1, short in1Off, short in1Len, 
        byte[] in2, short in2Off, short in2Len, 
        byte[] out, short outOff
    ) {
        final byte HMAC_IPAD = (byte) 0x36;
        final byte HMAC_OPAD = (byte) 0x5c;
        final short HMAC_BLOCK_SIZE = 64;
        final short HMAC_OUT_SIZE = 32;

        Util.arrayFillNonAtomic(tmpHmacSHA256, (short)0, HMAC_BLOCK_SIZE, HMAC_IPAD);
        for (short j = 0; j < keyLen; j++) {
            tmpHmacSHA256[(short)((short)0 + j)] ^= key[(short)(keyOff + j)];
        }
        sha256.reset();
        sha256.update(tmpHmacSHA256, (short)0, HMAC_BLOCK_SIZE);
        sha256.update(in1, in1Off, in1Len);
        sha256.doFinal(in2, in2Off, in2Len, out, outOff);

        for (short j = 0; j < HMAC_BLOCK_SIZE; j++) {
            tmpHmacSHA256[(short)((short)0 + j)] ^= HMAC_IPAD ^ HMAC_OPAD;
        }

        sha256.update(tmpHmacSHA256, (short)0, HMAC_BLOCK_SIZE);
        sha256.doFinal(out, outOff, HMAC_OUT_SIZE, out, outOff);

        Util.arrayFillNonAtomic(tmpHmacSHA256, (short)0, HMAC_BLOCK_SIZE, HMAC_IPAD);

        return HMAC_OUT_SIZE;
    }
}
