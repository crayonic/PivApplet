import pytest
import smartcard
import os
import hmac
import hashlib
import random
import string

from binascii import hexlify, unhexlify

import cryptography
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec, utils, padding
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, load_der_public_key
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from fido_applet import ApduRequest, FidoApplet
from fido_applet import PivApplet

backend = default_backend()
curve = ec.SECP256R1()

from test_base import *

ALLOWED = [
    PivApplet.INS_VERIFY,
    PivApplet.INS_CHANGE_PIN,
    PivApplet.INS_RESET_PIN,
    PivApplet.INS_GEN_AUTH,
    PivApplet.INS_GET_DATA,
    PivApplet.INS_PUT_DATA,
    PivApplet.INS_GEN_ASYM,
    PivApplet.INS_GET_RESPONSE,
    PivApplet.INS_SET_MGMT,
    PivApplet.INS_IMPORT_ASYM,
    PivApplet.INS_GET_VER,
    PivApplet.INS_RESET,
    PivApplet.INS_SET_PIN_RETRIES,
    PivApplet.INS_ATTEST,
    PivApplet.INS_GET_SERIAL,
    PivApplet.INS_GET_MDATA,

    FidoApplet.INS_FIDO_IMPORT_ENTROPY_ENCRYPTION_KEY, # Allowed for public use
    FidoApplet.INS_FIDO_INJECT_ATTESTATION_KEYPAIR, # Allowed for public use
]

BLOCKED = [
    FidoApplet.INS_FIDO_GENERATE_ENTROPY,
    FidoApplet.INS_FIDO_IMPORT_ENTROPY,
    FidoApplet.INS_FIDO_EXPORT_ENTROPY,
    FidoApplet.INS_FIDO_CTAP_MAKE_AUTH_TAG,
    FidoApplet.INS_FIDO_U2F_MAKE_AUTH_TAG,
    FidoApplet.INS_FIDO_DERIVE_KEY_AND_SIGN,
    FidoApplet.INS_FIDO_CTAP_NEW_KEY,
    FidoApplet.INS_FIDO_U2F_NEW_KEY,
    FidoApplet.INS_FIDO_GENERATE_CREDENTIAL_RANDOM,
    FidoApplet.INS_FIDO_GET_CREDENTIAL_MASK,
    # FidoApplet.INS_FIDO_INJECT_ATTESTATION_KEYPAIR,
    FidoApplet.INS_FIDO_SIGN_WITH_ATTESTATION_KEY,
    FidoApplet.INS_FIDO_GENERATE_RANDOM,
    FidoApplet.INS_FIDO_DERIVE_PUBLIC_KEY,
    # FidoApplet.INS_FIDO_IMPORT_ENTROPY_ENCRYPTION_KEY,
    FidoApplet.INS_DERIVE_USER_ENCRYPTION_KEY,
]

def test_applet_connection(unauthorized_applet: FidoApplet):
    pass


def test_piv_instructions_allowed(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet
    for ins in ALLOWED:
        response = applet.transmit(ApduRequest(
            0x00, ins,
            0x00,
            0x00,
        ))
        assert_not_sw(response, ISO7816.SW_INS_NOT_SUPPORTED)
        

def test_fido_instructions_blocked(unauthorized_applet: FidoApplet):
    applet = unauthorized_applet
    for ins in BLOCKED:
        response = applet.transmit(ApduRequest(
            0x00, ins,
            0x00,
            0x00,
        ))
        assert_sw(response, ISO7816.SW_INS_NOT_SUPPORTED)
        assert len(response.body) == 0

    